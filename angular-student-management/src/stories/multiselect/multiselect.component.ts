import { Component, EventEmitter, Input, Output } from '@angular/core';

import {SelectItem} from 'primeng/api';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-multiselect',
  templateUrl: './multiselect.component.html',
  styleUrls: ['./multiselect.component.scss']
})

export class MultiselectComponent{
  @Input() disabled = false;
  @Input() options: SelectItem[];
  @Input() filterable = true;
  @Input() filterBy: string = null;
  @Input() filterPlaceHolder = null;
  selectedItems: unknown[];

  @Output() selectedItemsChanged = new EventEmitter<unknown[]>();
}

