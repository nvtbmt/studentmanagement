import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { action } from '@storybook/addon-actions';

import { MultiSelectModule } from 'primeng/multiselect';

import { MultiselectComponent } from './multiselect.component';

const multiselectOptions = [
  { label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } },
  { label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } },
  { label: 'London', value: { id: 3, name: 'London', code: 'LDN' } },
  { label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } },
  { label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS' } },
];

export default {
    title: 'Multiselect',
    component: MultiselectComponent,
    args: {
        options: multiselectOptions,
        selectedItemsChanged: action('Selected items has changed')
    },
    decorators: [
        moduleMetadata({
            declarations: [],
            imports: [MultiSelectModule, BrowserAnimationsModule],
        }),
    ],
} as Meta;

const Template: Story<MultiselectComponent> = (args: MultiselectComponent) => ({
    component: MultiselectComponent,
    props: args
});

export const defaultMultiselect = Template.bind({});
defaultMultiselect.args = {
};
defaultMultiselect.storyName = 'Default Multiselect';

export const disabledMultiselect = Template.bind({});
disabledMultiselect.args = {
    disabled: true
};
disabledMultiselect.storyName = 'Disabled Multiselect';

export const disabledFilterMultiselect = Template.bind({});
disabledFilterMultiselect.args = {
    filterable: false
};
disabledFilterMultiselect.storyName = 'Disabled Filter Multiselect';

