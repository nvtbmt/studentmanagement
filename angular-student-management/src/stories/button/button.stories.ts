import { FormsModule } from '@angular/forms';

import { action } from '@storybook/addon-actions';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { boolean, text, withKnobs } from '@storybook/addon-knobs';

import { ButtonModule } from 'primeng/button';
import {ToggleButtonModule} from 'primeng/togglebutton';

import { ButtonComponent } from './button.component';

export default {
    title: 'Button',
    component: ButtonComponent,
    args: {
        clickEvent: action('Button was clicked'),
    },
    decorators: [
        withKnobs,
        moduleMetadata({
          imports: [ButtonModule, ToggleButtonModule, FormsModule],
        }),
    ]
} as Meta;

const Template: Story<ButtonComponent> = (args: ButtonComponent) => ({
    component: ButtonComponent,
    props: args
});

export const defaultButton = () => ({
    component: ButtonComponent,
    name: 'Default Button',
    props: {
      label: text('Label', 'Default Button'),
      isDisabled: boolean('should be disabled', false),
      clickEvent: action('Button was clicked'),
    },
});

export const disabledButton = Template.bind({});
disabledButton.args = {
    label: 'Disabled Button',
    isDisabled: true
};

export const warnButton = Template.bind({});
warnButton.args = {
    label: 'Warn Button',
    additionalClass: 'ui-button-warning'
};
warnButton.storyName = 'Severity button: Warn button';

export const roundedButton = Template.bind({});
roundedButton.args = {
    label: 'Rounded Button',
    additionalClass: 'ui-button-rounded'
};
roundedButton.storyName = 'Rounded button';

export const buttonWithIcon = Template.bind({});
buttonWithIcon.args = {
    label: 'Button with icon',
    icon: 'pi pi-check',
    iconPos: 'left'
};
buttonWithIcon.storyName = 'Button with icon';

export const toggleButton = Template.bind({});
toggleButton.args = {
    toggleButton: true,
    labels: ['Button is On', 'Button is Off'],
    icons: ['pi pi-check', 'pi pi-times'],
    iconPos: 'left'
};
toggleButton.storyName = 'toggle Button';
