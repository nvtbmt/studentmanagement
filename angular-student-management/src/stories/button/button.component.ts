import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'sb-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss']
})

export class ButtonComponent{
    /**
     * Is this the principal call to action on the page?
     */
    @Input()
    label: string;
    @Input() icon: string;
    @Input() iconPos: string;
    @Input() additionalClass: string;
    @Input() isDisabled = false;

    @Input() toggleButton = false;
    @Input() labels = ['', ''];
    @Input() icons = ['', ''];
    @Input() toggleButtonValue = true;

    @Output() clickEvent = new EventEmitter<Event>();
}

