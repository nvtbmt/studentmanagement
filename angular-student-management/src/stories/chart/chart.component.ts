import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'sb-chart',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.scss']
})

export default class ChartComponent{
    @Input() data: any;
    @Input() plugins: any[];
    @Input() width: string;
    @Input() height: string;
    @Input() responsive = true;

    @Input() bar = true;
    @Input() line: boolean;
    @Input() pie: boolean;
    @Input() doughtnut: boolean;

    @Input() displayTitle = false;
    @Input() title = '';
    @Input() legendPosition = 'top';


    // tslint:disable:no-output-on-prefix
    @Output() onDataSelect = new EventEmitter<Event>();

    public get type(): string {
        if (this.line){
            return 'line';
        }
        if (this.pie){
            return 'pie';
        }
        if (this.doughtnut){
            return 'doughtnut';
        }
        return 'bar';
    }

    public get options(): any {
        return {
            title: {
                display: true,
                text: this.title
            },
            legend: {
                position: this.legendPosition
            }
        };
    }
}

