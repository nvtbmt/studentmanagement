import { Meta, moduleMetadata, storiesOf, Story } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';

import { ButtonModule } from 'primeng/button';
import {ToggleButtonModule} from 'primeng/togglebutton';

import ChartComponent from './chart.component';
import { ChartModule } from 'primeng-lts/chart';

const chartData = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
        {
            label: 'First Dataset',
            backgroundColor: 'orange',
            data: [65, 59, 80, 81, 56, 55, 40],
            fill: false,
        },
        {
            label: 'Second Dataset',
            backgroundColor: '#42A5F5',
            data: [28, 48, 40, 19, 86, 27, 90],
            fill: false,
        }
    ]
};

export default {
    title: 'Chart',
    component: ChartComponent,
    args: {
        data: chartData,
        width: '400px',
        height: '300px',
        onDataSelect: action('Charts element was clicked'),
    },
    decorators: [
        moduleMetadata({
          imports: [ChartModule],
        }),
    ]
} as Meta;

const Template: Story<ChartComponent> = (args: ChartComponent) => ({
    component: ChartComponent,
    props: args
});

export const barChart = Template.bind({});
barChart.args = {
    bar: true
};
barChart.storyName = 'Bar chart';

export const pieChart = Template.bind({});
pieChart.args = {
    pie: true
};
pieChart.storyName = 'Pie chart';

export const lineChart = Template.bind({});
lineChart.args = {
    line: true
};
lineChart.storyName = 'Line chart';
