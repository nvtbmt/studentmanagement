import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { action } from '@storybook/addon-actions';

import { ButtonModule } from 'primeng/button';
import { ToggleButtonModule } from 'primeng/togglebutton';

import { TopBarComponent } from './top-bar.component';
import { ButtonComponent } from '../button/button.component';

export default {
  title: 'Top Bar',
  component: TopBarComponent,
  subcomponents: {ButtonComponent},
  args: {
    homeUrl: '/',
    siteName: 'STUDENT MANAGEMENT',
    toggleMenu: action('Toggle menu'),
    logIn: action('Logged In'),
    logOut: action('Logged out'),
  },
  decorators: [
    moduleMetadata({
      declarations: [ButtonComponent],
      imports: [RouterModule, ButtonModule, RouterTestingModule, ToggleButtonModule],
    }),
  ],
} as Meta;

const Template: Story<TopBarComponent> = (args: TopBarComponent) => ({
  component: TopBarComponent,
  props: args
});

export const loggedInTopBar = Template.bind({});
loggedInTopBar.args = {
  user: {}
};
loggedInTopBar.storyName = 'Logged In Top Bar';

export const loggedOutTopBar = Template.bind({});
loggedOutTopBar.args = {
};
loggedOutTopBar.storyName = 'Logged Out Top Bar';
