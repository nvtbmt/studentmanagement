import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})

export class TopBarComponent{
  @Input() homeUrl: string;
  @Input() siteName: string;
  @Input() user: any = null;

  @Output() toggleMenu = new EventEmitter<boolean>();
  @Output() logIn = new EventEmitter<Event>();
  @Output() logOut = new EventEmitter<Event>();

  value = true;
}

