import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

import {SelectItem} from 'primeng/api';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-inputgroup',
  templateUrl: './inputgroup.component.html',
  styleUrls: ['./inputgroup.component.scss']
})

export class InputGroupComponent implements OnInit{
  readonly inputTypes = ['text', 'number', 'textarea', 'dropdown', 'multiselect', 'checkbox', 'radio', 'switch', 'calendar'];
  @Input() inputType = 'text';
  @Input() floatLabel = false;
  @Input() additionalClass: string;
  @Input() inputId: string;
  @Input() label: string;
  @Input() keyFilter: string;
  @Input() validationErrorDetail: string[];
  @Input() formCtrl: FormControl = new FormControl();
  @Input() failedValidation = false;
  @Input() disabled = false;

  // input number
  @Input() useGrouping = false;
  @Input() showButtons = false;
  @Input() min: number;
  @Input() max: number;

  // text area
  @Input() rows: number;
  @Input() cols: number;

  // dropdown
  @Input() showClear = false;
  @Input() options: SelectItem[];
  @Input() editable = false;
  @Input() filterable = false;
  @Input() filterBy: string = null;
  @Input() group = false;
  @Input() placeholder = 'Select an item';

  // radio
  @Input() name = 'radioGroupName';
  @Input() cases: RadioButtonCase[];

  // switch
  @Input() block = false;

  // calendar
  @Input() showTime = false;
  @Input() showButtonBar = false;
  @Input() disabledPastDate = true;
  @Input() monthNavigator = false;
  @Input() yearNavigator = false;
  @Input() yearRange = (new Date().getFullYear() - 20).toString() + ':' + new Date().getFullYear().toString();
  @Input() minDate: Date;
  @Input() maxDate: Date;
  currentDate = new Date();
  @Output() valueChanged = new EventEmitter();

  checkFailedValidation(){
    this.failedValidation = this.formCtrl.invalid  && (this.formCtrl.dirty || this.formCtrl.touched);
    // console.warn(this.failedValidation);
    // console.warn(this.formCtrl.invalid);
  }

  ngOnInit(){
    this.formCtrl.valueChanges.subscribe(x => {
      this.checkFailedValidation();
      this.valueChanged.emit(null);
    });
  }
}

export interface RadioButtonCase{
  label: string;
  value: string;
}

export interface ValidateErrorDetail{
  condition: boolean;
  errorText: string;
}

