import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Meta, moduleMetadata, Story } from '@storybook/angular';

// import { InputTextareaModule } from 'primeng/inputtextarea';
// import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';

import { InputGroupComponent } from './inputgroup.component';
import { InputTextComponent } from '../inputText/inputText.component';

export default {
  title: 'Input Group',
  component: InputGroupComponent,
  subcomponents: { InputTextComponent },
  args: {
    inputId: 'input',
    label: 'Label: ',
  },
  decorators: [
    moduleMetadata({
      declarations: [InputTextComponent],
      imports: [BrowserAnimationsModule, FormsModule, ReactiveFormsModule,
        InputTextModule, KeyFilterModule,
        //  InputNumberModule, InputTextareaModule
      ],
    }),
  ],
} as Meta;

const Template: Story<InputGroupComponent> = (args: InputGroupComponent) => ({
  component: InputGroupComponent,
  props: args
});

export const defaultInputGroup = Template.bind({});
defaultInputGroup.args = {
};
defaultInputGroup.storyName = 'Default Input Text';

export const failedValidationInputGroup = Template.bind({});
failedValidationInputGroup.args = {
  validate: true,
  failedValidation: true,
  validationErrorDetail: [
    'validation failed due to reason1',
    'validation failed due to reason2'
  ]
};
failedValidationInputGroup.storyName = 'Failed Validation Input Group';
