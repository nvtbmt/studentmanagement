import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Meta, moduleMetadata, Story } from '@storybook/angular';

import { MenuModule } from 'primeng-lts/menu';
import { MenubarModule } from 'primeng-lts/menubar';

import { MenuComponent } from './menu.component';

const menuItems = [
    {
      label: 'Home Page', icon: 'pi pi-fw pi-home',
      items: [
        {label: 'Dashboard', routerLink: ['/']},
        {label: 'Statistics', routerLink: ['/']}
      ]
    },
    {
      label: 'Group 1',
      items: [
        {label: 'Item 1', routerLink: ['/']},
        {label: 'Item 2', routerLink: ['/']},
        {label: 'Item 3', routerLink: ['/']}
      ]
    },
    {
      label: 'Group 2',
      items: [
        {label: 'Item 1', routerLink: ['/']},
        {label: 'Item 2', routerLink: ['/']},
        {label: 'Item 3', routerLink: ['/']}
      ]
    },
    {
      label: 'Group 3',
      items: [
        {label: 'Item 1', routerLink: ['/']},
        {label: 'Item 2', routerLink: ['/']},
        {label: 'Item 3', routerLink: ['/']}
      ]
    },
];

export default {
    title: 'Menu',
    component: MenuComponent,
    args: {
        // tslint:disable: object-literal-shorthand
        menuItems: menuItems
    },
    decorators: [
        moduleMetadata({
            declarations: [],
            imports: [MenuModule, MenubarModule, BrowserAnimationsModule, RouterTestingModule],
        }),
    ],
} as Meta;

const Template: Story<MenuComponent> = (args: MenuComponent) => ({
    component: MenuComponent,
    props: args
});

export const verticalMenu = Template.bind({});
verticalMenu.args = {
    verticalMenu: true
};
verticalMenu.storyName = 'Vertical Menu';

export const horizontalMenu = Template.bind({});
horizontalMenu.args = {
    verticalMenu: false
};
horizontalMenu.storyName = 'Horizontal Menu';
