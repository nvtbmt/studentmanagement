import { Component, Input } from '@angular/core';

import {MenuItem} from 'primeng/api';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuComponent{
  @Input() menuItems: MenuItem[];
  @Input() verticalMenu = false;
}

