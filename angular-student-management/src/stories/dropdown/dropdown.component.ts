import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

import {SelectItem} from 'primeng/api';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'sb-dropdown',
    templateUrl: './dropdown.component.html',
    styleUrls: ['./dropdown.component.scss']
})

export class DropdownComponent{
    @Input() disabled = false;
    @Input() showClear = false;
    @Input() options: SelectItem[];
    @Input() editable = false;
    @Input() filterable = false;
    @Input() filterBy: string = null;
    @Input() group = false;
    @Input() placeholder = 'Select an item';
    @Input() formCtrl: FormControl;
    selectedItem: unknown;

    @Output() selectedItemChanged = new EventEmitter<unknown>();
}

