import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormControl, ReactiveFormsModule } from '@angular/forms';

import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { action } from '@storybook/addon-actions';

import { DropdownModule } from 'primeng/dropdown';

import { DropdownComponent } from './dropdown.component';

const dropdownOptions = [
  { label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } },
  { label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } },
  { label: 'London', value: { id: 3, name: 'London', code: 'LDN' } },
  { label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } },
  { label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS' } },
];
const dropdownGroupedOptions = [
  {
    label: 'USA',
    items: [
      { label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } },
      {
        label: 'Washington',
        value: { id: 0, name: 'Washington', code: 'WST' },
      },
    ],
  },
  {
    label: 'EU',
    items: [
      { label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } },
      { label: 'London', value: { id: 3, name: 'London', code: 'LDN' } },
      { label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } },
      { label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS' } },
    ],
  },
];

export default {
    title: 'Dropdown',
    component: DropdownComponent,
    args: {
        options: dropdownOptions,
        showClear: true,
        formCtrl: new FormControl(),
        selectedItemChanged: action('Selected item has changed')
    },
    decorators: [
        moduleMetadata({
            declarations: [],
            imports: [DropdownModule, BrowserAnimationsModule, ReactiveFormsModule],
        }),
    ],
} as Meta;

const Template: Story<DropdownComponent> = (args: DropdownComponent) => ({
    component: DropdownComponent,
    props: args
});

export const defaultDropdown = Template.bind({});
defaultDropdown.args = {
};
defaultDropdown.storyName = 'Default Dropdown';

export const disabledDropdown = Template.bind({});
disabledDropdown.args = {
    disabled: true
};
disabledDropdown.storyName = 'Disabled Dropdown';

export const editableDropdown = Template.bind({});
editableDropdown.args = {
    editable: true
};
editableDropdown.storyName = 'Editable Dropdown';

export const filterableDropdown = Template.bind({});
filterableDropdown.args = {
    filterable: true,
    filterBy: 'value.name'
};
filterableDropdown.storyName = 'Filterable Dropdown';

export const groupingDropdown = Template.bind({});
groupingDropdown.args = {
    options: dropdownGroupedOptions,
    group: true
};
groupingDropdown.storyName = 'Grouping Dropdown';

export const groupAndFilterableDropdown = Template.bind({});
groupAndFilterableDropdown.args = {
    options: dropdownGroupedOptions,
    group: true,
    filterable: true
};
groupAndFilterableDropdown.storyName = 'Filterable Grouping Dropdown';
