import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { HttpClientModule } from '@angular/common/http';

import { TableModule } from 'primeng-lts/table';
import { MultiSelectModule } from 'primeng-lts/multiselect';

import { DatatablelibComponent } from 'datatablelib';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from 'primeng-lts/button';

export default {
    title: 'Standard Table',
    component: DatatablelibComponent,
    decorators: [
        moduleMetadata({
          imports: [TableModule, MultiSelectModule, HttpClientModule, BrowserAnimationsModule, ButtonModule],
        }),
    ]
} as Meta;

const Template: Story<DatatablelibComponent> = (args: DatatablelibComponent) => ({
    component: DatatablelibComponent,
    props: args
});

export const singleTable = Template.bind({});
singleTable.args = {
    endPoint: 'https://studentmanagementapi.azurewebsites.net/api/Faculty/Get',
    pickedProperties: [
        { field: 'id', header: 'Id' },
        { field: 'facultyName', header: 'Faculty Name' },
        { field: 'trainingTime', header: 'Training Time' }
    ],
    caption: 'Faculties'
};
singleTable.storyName = 'Single Table';

export const multiFeaturesTable = Template.bind({});
multiFeaturesTable.args = {
    endPoint: 'https://studentmanagementapi.azurewebsites.net/api/Faculty/Get',
    pickedProperties: [
        { field: 'id', header: 'Id' },
        { field: 'facultyName', header: 'Faculty Name' },
        { field: 'trainingTime', header: 'Training Time' }
    ],
    caption: 'Faculties',
    searchTool: true,
    columnToggle: true,
    paging: true,
    rowPaging: 5,
    exportType: ['excel', 'csv']
};
multiFeaturesTable.storyName = 'Feature Table';
