import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';

import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { action } from '@storybook/addon-actions';

import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ToggleButtonModule } from 'primeng/togglebutton';

import { StudentFormComponent } from './studentform.component';
import { InputGroupComponent } from '../inputgroup/inputgroup.component';
import { InputTextComponent } from '../inputText/inputText.component';
import { DropdownComponent } from '../dropdown/dropdown.component';
import { ButtonComponent } from '../button/button.component';

export default {
  title: 'Student Form',
  component: StudentFormComponent,
  subcomponents: {InputGroupComponent, DropdownComponent, ButtonComponent},
  args: {
    selectedItemChanged: action('Selected item has changed'),
    submitForm: action('form has been submited')
  },
  decorators: [
    moduleMetadata({
      declarations: [
        InputGroupComponent,
        InputTextComponent,
        DropdownComponent,
        ButtonComponent,
      ],
      imports: [
        InputTextModule,
        KeyFilterModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        DropdownModule,
        ButtonModule,
        ToggleButtonModule,
        RadioButtonModule
      ],
    }),
  ]
} as Meta;

const Template: Story<StudentFormComponent> = (args: StudentFormComponent) => ({
    component: StudentFormComponent,
    props: args
});

export const emptyStudentForm = Template.bind({});
emptyStudentForm.args = {
};
emptyStudentForm.storyName = 'Empty Student Form';

export const studentFormWithValues = Template.bind({});
studentFormWithValues.args = {
    firstNameControl: new FormControl('Joao', Validators.required),
    lastNameControl: new FormControl('Felix', Validators.required),
    genderControl: new FormControl('Male', Validators.required),
    facultyControl: new FormControl('Faculty of Information Systems', Validators.required)
};
studentFormWithValues.storyName = 'Student Form With Values';
