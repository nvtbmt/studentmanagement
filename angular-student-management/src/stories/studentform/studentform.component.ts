import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { SelectItem } from 'primeng/api';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-studentForm',
  templateUrl: './studentform.component.html',
  styleUrls: ['./studentform.component.scss'],
})
export class StudentFormComponent implements OnChanges {
  @Input() firstNameControl = new FormControl('', Validators.required);
  @Input() lastNameControl = new FormControl('', Validators.required);
  @Input() genderControl = new FormControl('', Validators.required);
  @Input() facultyControl = new FormControl('', Validators.required);
  @Input() inputControls = [
    {
      inputId: 'firstName',
      label: 'First Name (*)',
      formControl: this.firstNameControl,
      pKeyFilter: 'alpha',
      validate: true,
      validationErrorDetail: [
        'First name is required',
      ],
    },
    {
      inputId: 'lastName',
      label: 'Last Name (*)',
      formControl: this.lastNameControl,
      pKeyFilter: 'alpha',
      validate: true,
      validationErrorDetail: [
        'Last name is required',
      ],
    },
  ];
  @Input() faculties: SelectItem[] = [
    { label: 'Faculty of Information Systems', value: 'Faculty of Information Systems' }
  ];

  @Output() submitForm = new EventEmitter<unknown>();

  studentForm: FormGroup;
  formBuilder: FormBuilder = new FormBuilder();

  ngOnChanges(){
    this.studentForm = this.formBuilder.group({
      firstName: this.firstNameControl,
      lastName: this.lastNameControl,
      gender: this.genderControl,
      faculty: this.facultyControl,
    });
  }
}

