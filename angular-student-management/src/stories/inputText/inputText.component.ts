import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-inputText',
  templateUrl: './inputText.component.html',
  styleUrls: ['./inputText.component.scss']
})

export class InputTextComponent implements OnChanges{
  @Input() disabled = false;
  @Input() inputId: string = null;
  @Input() formControlDirective: string = null;
  @Input() keyFilter: string;
  @Input() useInsideFormGroup = false;
  @Input() formCtrl: FormControl = new FormControl();

  @Output() formCtrlInvalidStatusChange = new EventEmitter<boolean>();

  bidingValue: string;
  constructor(){
  }

  ngOnChanges(){
    this.formCtrl.valueChanges.subscribe(x => {
      this.formCtrlInvalidStatusChange.emit(this.formCtrl.invalid  && (this.formCtrl.dirty || this.formCtrl.touched));
    });
  }
}

