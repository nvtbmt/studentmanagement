import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Meta, moduleMetadata, Story } from '@storybook/angular';

import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';

import { InputTextComponent } from './inputText.component';

export default {
    title: 'Input Text',
    component: InputTextComponent,
    args: {

    },
    decorators: [
        moduleMetadata({
            declarations: [],
            imports: [InputTextModule, KeyFilterModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule],
        }),
    ],
} as Meta;

const Template: Story<InputTextComponent> = (args: InputTextComponent) => ({
    component: InputTextComponent,
    props: args
});

export const defaultInputText = Template.bind({});
defaultInputText.args = {
};
defaultInputText.storyName = 'Default Input Text';

export const keyFilterInputText = Template.bind({});
keyFilterInputText.args = {
    keyFilter: 'alpha'
};
keyFilterInputText.storyName = 'Key Filter Input Text';

export const disabledInputText = Template.bind({});
disabledInputText.args = {
    disabled: true
};
disabledInputText.storyName = 'Disabled Input Text';

export const formControlInputText = Template.bind({});
formControlInputText.args = {
    formControlDirective: '[formControlName]="formControlName"'
};
formControlInputText.storyName = 'FormControl Input Text';
