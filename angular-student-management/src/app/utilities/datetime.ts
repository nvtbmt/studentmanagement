import { AbstractControl, ValidatorFn } from '@angular/forms';

export function keepTimeFromTimezoneOffset(dateTime: Date): Date{
  dateTime.setHours(dateTime.getHours() + (dateTime.getTimezoneOffset() / -60));
  return dateTime;
}

export function dateGtDateValidator(dateControlOrDateValue: Date | AbstractControl, acceptNull = false): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const thisDate = control.value as Date;
    const comparedDate = 'getDate' in dateControlOrDateValue ? dateControlOrDateValue : (dateControlOrDateValue.value as Date);
    return (thisDate > comparedDate || (acceptNull && (comparedDate === null || thisDate === null))) ?
      null : { invalidDate: { value: control.value } };  // return null if passed
  };
}

export function dateLtDateValidator(dateControlOrDateValue: Date | AbstractControl, acceptNull = false): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const thisDate = control.value as Date;
    const comparedDate = 'getDate' in dateControlOrDateValue ? dateControlOrDateValue : (dateControlOrDateValue.value as Date);
    return (thisDate < comparedDate || (acceptNull && (comparedDate === null || thisDate === null))) ?
      null : {invalidDate: {value: control.value}};
  };
}
