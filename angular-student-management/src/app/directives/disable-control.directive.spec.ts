import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormControlDirective, FormsModule, NgControl, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { DisableControlDirective } from './disable-control.directive';

@Component({
  template: `
  <input type="text" [disableControl]="true" id="disabled">
  <input type="text" [disableControl]="false" id="enabled">
  `
})
class TestComponent {}

describe('DisableControlDirective', () => {
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      imports: [ FormsModule, ReactiveFormsModule ],
      declarations: [DisableControlDirective, TestComponent],
      providers: [ TestComponent, ]
    }).createComponent(TestComponent);

    fixture.detectChanges();
  });

  // it('should create an instance', () => {
  //   const hasDirective = fixture.debugElement.queryAll(By.directive(DisableControlDirective));
  //   const disabled = fixture.debugElement.queryAll(By.css('#disabled[disabled]'));
  //   const enabled = fixture.debugElement.queryAll(By.css('#enabled[enabled]'));
  //   expect(hasDirective.length).toEqual(2);
  //   expect(disabled.length).toEqual(1);
  //   expect(enabled.length).toEqual(1);
  // });
});
