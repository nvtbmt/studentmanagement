import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MenuModule } from 'primeng/menu';
import { ChartModule } from 'primeng/chart';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { SidebarModule } from 'primeng/sidebar';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';
import { CalendarModule } from 'primeng/calendar';
import { MessageService } from 'primeng/api';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
// import { KeyFilterModule } from 'primeng/keyfilter';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { ToggleButtonModule } from 'primeng/togglebutton';

import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { AppRoutingModule } from './app-routing.module';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ActivitiesComponent } from './activities/activities.component';

import { ButtonComponent } from 'src/stories/button/button.component';
import { DropdownComponent } from 'src/stories/dropdown/dropdown.component';
import { InputTextComponent } from 'src/stories/inputtext/inputText.component';
import { InputGroupComponent } from 'src/stories/inputgroup/inputgroup.component';
import { DisableControlDirective } from './directives/disable-control.directive';

import ChartComponent from 'src/stories/chart/chart.component';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    LeftMenuComponent,
    DashboardComponent,
    ActivitiesComponent,
    InputTextComponent,
    InputGroupComponent,
    DropdownComponent,
    ButtonComponent,
    DisableControlDirective,
    ChartComponent
  ],
  imports: [
    AppRoutingModule,
    ButtonModule,
    BrowserModule,
    BrowserAnimationsModule,
    CalendarModule,
    ChartModule,
    DialogModule,
    DropdownModule,
    FullCalendarModule,
    FormsModule,
    HttpClientModule,
    InputNumberModule,
    InputSwitchModule,
    InputTextModule,
    InputTextModule,
    // KeyFilterModule,
    MenuModule,
    MessageModule,
    MessagesModule,
    RadioButtonModule,
    ReactiveFormsModule,
    SidebarModule,
    ToggleButtonModule,
    ToastModule,
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
