import { Injectable } from '@angular/core';
import { defer, Observable, of, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Faculty } from '../interfaces/faculty.model';
import { Activity } from '../interfaces/activity.model';
import { Department } from '../interfaces/department.model';
import { StudentStatistics } from '../interfaces/student-statistics.model';
import { keepTimeFromTimezoneOffset } from '../utilities/datetime';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  baseUrl = 'https://studentmanagementapi.azurewebsites.net/api/';
  faculties: Faculty[];
  departments: Department[];
  activities: Activity[];
  facultyNames: string[];
  departmentNames: string[];

  constructor(private http: HttpClient) { }

  getStudentStatistics(): Observable<StudentStatistics[]>{
    return this.http.get<StudentStatistics[]>(this.baseUrl + 'Student/getStatistics')
    .pipe(
      retry(3),
      catchError(this.handleError<StudentStatistics[]>('get student statistics', []))
    );
  }

  getFaculties(): Observable<Faculty[]> {
    return this.http
      .get<Faculty[]>(this.baseUrl + 'Faculty/Get')
      .pipe(
        retry(3),
        catchError(this.handleError<Faculty[]>('get faculty', []))
      );
  }

  getDepartments(): Observable<Department[]> {
    return this.http
      .get<Department[]>(this.baseUrl + 'Department/Get')
      .pipe(
        retry(3),
        catchError(this.handleError<Department[]>('get departments', []))
      );
  }

  getActivities(): Observable<Activity[]>{
    return this.http.get<Activity[]>(this.baseUrl + 'Activity/Get')
    .pipe(
      map((activities) => {
        activities.forEach((element) => {
          element.startDate = new Date(element.startDate);
          element.endDate = new Date(element.endDate);
        });
        return activities;
      }),
      retry(3),
      catchError(this.handleError<Activity[]>('get activities', []))
    );
  }

  addActivity(activity: Activity): Observable<Activity>{
    if (activity.startDate){
      activity.startDate = keepTimeFromTimezoneOffset(activity.startDate);
    }
    if (activity.endDate){
      activity.endDate = keepTimeFromTimezoneOffset(activity.endDate);
    }

    return this.http.post<Activity>(this.baseUrl + 'Activity/Post', activity)
      .pipe(
        retry(3),
        catchError(this.handleError<Activity>('post activities', activity))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} error: ${error.message}`);
      return throwError({
        message: error.message,
        object: result
      });
    };
  }
}
