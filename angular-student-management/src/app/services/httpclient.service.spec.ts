import { HttpClient, HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { defer, of, throwError } from 'rxjs';

import { StudentStatistics } from '../interfaces/student-statistics.model';
import { HttpClientService } from './httpclient.service';

describe('HttpclientService', () => {
  let service: HttpClientService;
  let http: { get: jasmine.Spy };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpClient]
    });
    // service = TestBed.inject(HttpclientService);
    http = jasmine.createSpyObj('HttpClient', ['get']);
    service = new HttpClientService(http as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return expected values', () => {
    const expectedStudentStatistics: StudentStatistics[] = [
      {year: 2019, total: 30, outOfDateTotal: 10},
      {year: 2020, total: 40, outOfDateTotal: 15}
    ];

    http.get.and.returnValue(of(expectedStudentStatistics));

    service.getStudentStatistics().subscribe(
      studentStatistics => expect(studentStatistics).toEqual(expectedStudentStatistics),
      error => fail('should have return data')
    );
  });

  it('should return an error when the server return a 404 status', () => {

    http.get.and.returnValue(throwError({
      message: 'test 404 error'
    }));

    service.getStudentStatistics().subscribe(
      data => fail('expected an error'),
      error => expect(error.message).toEqual('test 404 error')
    );
  });

});
