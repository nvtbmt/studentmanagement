export interface Department {
  id: string;
  departmentName: string;
  facultyId: string;
  facultyName: string;
}
