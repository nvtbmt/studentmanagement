export interface Activity {
  id: string;
  activityName: string;
  organizationalFaculty?: string;
  organizationalFacultyName?: string;
  organizationalDepartment?: string;
  organizationalDepartmentName?: string;
  isRequired: boolean;
  point?: number;
  startDate: Date;
  endDate?: Date;
  finished: boolean;
  participants?: Participant[];
}

export interface Participant {
  studentId: string;
  participated?: boolean;
  achievedScore?: number;
}

export interface ParticipantModel {
  activityId: string;
  studentIds: string[];
}
