export interface Faculty {
  id: string;
  facultyName: string;
  trainingTime: number;
}
