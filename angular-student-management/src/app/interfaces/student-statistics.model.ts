export interface StudentStatistics {
  year: number;
  total: number;
  outOfDateTotal: number;
}
