import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { ChartModule } from 'primeng-lts/chart';

import { HttpClientService } from '../services/httpclient.service';
import { DashboardComponent } from './dashboard.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  const service = jasmine.createSpyObj('HttpClientService', ['getStudentStatistics']);
  const mockData = [
    { year: 2020, total: 10, outOfDateTotal: 5 },
    { year: 2021, total: 20, outOfDateTotal: 7 }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardComponent ],
      imports: [HttpClientTestingModule, ChartModule],
      providers: [ DashboardComponent,
        { provide: HttpClientService, useValue: service }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    service.getStudentStatistics.and.returnValue(of(mockData));
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should setup data when initialing', () => {
    expect(component.chartData.labels).toEqual( [2020, 2021] );
    expect(component.chartData.datasets[0].data).toEqual( [5, 7] );
    expect(component.chartData.datasets[1].data).toEqual( [10, 20] );
  });
});
