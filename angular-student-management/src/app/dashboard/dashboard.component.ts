import { Component, OnDestroy, OnInit } from '@angular/core';

import { StudentStatistics } from '../interfaces/student-statistics.model';
import { HttpClientService } from '../services/httpclient.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  chartData: any;
  studentStatistics: StudentStatistics[];

  constructor(private httpclient: HttpClientService) { }

  ngOnInit(): void {
    this.httpclient.getStudentStatistics()
    .subscribe((studentStatistics) => {
      const years = [];
      const total = [];
      const outOfDateTotal = [];
      studentStatistics.forEach(element => {
        years.push(element.year);
        total.push(element.total);
        outOfDateTotal.push(element.outOfDateTotal);
      });
      this.chartData = {
        labels: years,
        datasets: [
          {
            label: 'Total Out of Training time Students',
            backgroundColor: 'orange',
            data: outOfDateTotal
          },
          {
            label: 'Total Students',
            backgroundColor: '#42A5F5',
            data: total
          }
        ]
      };
    });

  }

}
