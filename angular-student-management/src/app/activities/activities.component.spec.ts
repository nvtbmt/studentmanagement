import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessageService } from 'primeng-lts/api';
import { DialogModule } from 'primeng-lts/dialog';
import { FullCalendarModule } from 'primeng-lts/fullcalendar';
import { ToastModule } from 'primeng-lts/toast';
import { of } from 'rxjs';
import { HttpClientService } from '../services/httpclient.service';

import { ActivitiesComponent } from './activities.component';

describe('ActivitiesComponent', () => {
  let component: ActivitiesComponent;
  let fixture: ComponentFixture<ActivitiesComponent>;
  const service = jasmine.createSpyObj('HttpClientService', ['getActivities', 'getFaculties', 'getDepartments']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivitiesComponent ],
      imports: [ToastModule, FormsModule, ReactiveFormsModule, FullCalendarModule, DialogModule,
        ],
      providers: [{provide: HttpClientService, useValue: service},
        FormBuilder, MessageService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    service.getActivities.and.returnValue(of([{id: 'id', activityName: 'aname'}]));
    service.getFaculties.and.returnValue(of([{id: 'id', facultyName: 'fname'}]));
    service.getDepartments.and.returnValue(of([{id: 'id', departmentName: 'dname'}]));
    fixture = TestBed.createComponent(ActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const f = TestBed.inject(HttpClientService);
    console.log(f);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    const f = TestBed.inject(HttpClientService);
    f.activities = [...f.activities, {id: 'id2', activityName: 'name2', startDate: new Date(), isRequired: false, finished: false}];
    component.f();
  });
});
