import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

import { MessageService, SelectItem } from 'primeng/api';

import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';

import { Activity } from '../interfaces/activity.model';
import { HttpClientService } from '../services/httpclient.service';
import { InputGroupComponent } from 'src/stories/inputgroup/inputgroup.component';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent implements OnInit {

  @ViewChildren(InputGroupComponent) inputGroups: QueryList<InputGroupComponent>;

  events: any[];
  options: any;
  activityForm: FormGroup;
  facultyOptions: SelectItem[];
  departmentOptions: SelectItem[];
  activityNameValidation = ['Activity name is required'];
  startDateValidation = ['Start date is required', 'Start date should be before End date'];
  endDateValidation = ['End date should be after Start date'];
  selectedActivity: Activity;
  displayDialog = false;

  constructor(
    private http: HttpClientService,
    private formBuilder: FormBuilder,
    private messageService: MessageService
  ) {
    this.setupForm();
  }

  f(){
    console.warn(this.http.activities);
  }

  ngOnInit(): void {
    console.warn(this.http);
    this.getActivities();
    this.options = {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      eventClick: (info) => {
        const activityId = info.event.id;
        this.selectedActivity = this.http.activities.filter(x => x.id === activityId)[0];
        this.setupForm(this.selectedActivity);
        this.displayDialog = true;
      }
    };

    this.http.getFaculties().subscribe((faculties) => {
      this.http.faculties = faculties;
      this.facultyOptions = [];
      faculties.forEach(faculty => {
        this.facultyOptions.push({label: faculty.facultyName, value: faculty});
      });
    });

    this.http.getDepartments().subscribe((departments) => {
      this.http.departments = departments;
      this.departmentOptions = [];
      departments.forEach(department => {
        this.departmentOptions.push({label: department.departmentName, value: department});
      });
    });
  }

  setupForm(activity?: Activity){
    if (!activity){
      this.activityForm = this.formBuilder.group({
        id: new FormControl(''),
        activityName: new FormControl('', Validators.required),
        organizationalFaculty: new FormControl(null),
        organizationalDepartment: new FormControl(null),
        isRequired: new FormControl(false, Validators.required),
        point: new FormControl(0),
        startDate: new FormControl(new Date(), Validators.required),
        endDate: new FormControl(null),
        finished: new FormControl(false, Validators.required),
        participants: new FormControl([])
      });
    }
    else{
      this.activityForm = this.formBuilder.group({
        id: new FormControl(activity.id),
        activityName: new FormControl(activity.activityName, Validators.required),
        organizationalFaculty: new FormControl(activity.organizationalFaculty),
        organizationalDepartment: new FormControl(activity.organizationalDepartment),
        isRequired: new FormControl(activity.isRequired, Validators.required),
        point: new FormControl(activity.point),
        startDate: new FormControl(activity.startDate, Validators.required),
        endDate: new FormControl(activity.endDate),
        finished: new FormControl(activity.finished, Validators.required),
        participants: new FormControl(activity.participants)
      });
    }
    this.activityForm.get('startDate').setValidators([
      Validators.required,
      dateLtDateValidator(this.activityForm.get('endDate'), true)
    ]);
    this.activityForm.get('endDate').setValidators([
      dateGtDateValidator(this.activityForm.get('startDate'), true)
    ]);
  }

  getActivities(){
    const tempEvents = [];
    this.http.getActivities().subscribe((activities) => {
      this.http.activities = activities;
      activities.forEach(activity => {
        tempEvents.push({
          id: activity.id,
          title: activity.activityName,
          start: activity.startDate,
          end: activity.endDate
        });
      });
      this.events = tempEvents;
    });
  }

  checkValidation(event){
    if (this.inputGroups){
      this.inputGroups.forEach(inputGroup => {
        inputGroup.checkFailedValidation();
      });
    }
  }

  onSubmit() {
    const participantsId: string[] = this.activityForm.get('participants').value;
    const activity: Activity = {
      id: this.activityForm.get('organizationalFaculty').value,
      activityName: this.activityForm.get('activityName').value,
      isRequired: this.activityForm.get('isRequired').value,
      point: this.activityForm.get('point').value,
      startDate: this.activityForm.get('startDate').value,
      endDate: this.activityForm.get('endDate').value,
      finished: this.activityForm.get('finished').value,
      participants: participantsId.map((id) => {
        return { studentId: id };
      })
    };
    if (this.activityForm.get('organizationalFaculty').value !== null){
      activity.organizationalFaculty = this.activityForm.get('organizationalFaculty').value.id;
      activity.organizationalFacultyName = this.activityForm.get('organizationalFaculty').value.facultyName;
    }
    if (this.activityForm.get('organizationalDepartment').value !== null){
      activity.organizationalDepartment = this.activityForm.get('organizationalDepartment').value.id;
      activity.organizationalDepartmentName = this.activityForm.get('organizationalDepartment').value.departmentName;
    }

    this.http.addActivity(activity).subscribe(() => {
      this.messageService.add({
        severity: 'success',
        summary: 'Activity added successfully',
        detail: activity.activityName
      });
      this.activityForm.reset();
      this.getActivities();
    });
  }

}

export function dateGtDateValidator(dateControl: AbstractControl, acceptNull = false): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const thisDate = control.value as Date;
    return (thisDate > (dateControl.value as Date) || (acceptNull && (dateControl.value === null || thisDate === null))) ?
      null : { invalidDate: { value: control.value } };  // return null if passed
  };
}

export function dateLtDateValidator(dateControl: AbstractControl, acceptNull = false): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const thisDate = control.value as Date;
    return (thisDate < (dateControl.value as Date) || (acceptNull && (dateControl.value === null || thisDate === null))) ?
      null : {invalidDate: {value: control.value}};
  };
}
