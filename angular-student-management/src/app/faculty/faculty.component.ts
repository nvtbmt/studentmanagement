import { Component, OnInit } from '@angular/core';

import { ColumnItems } from 'datatablelib';

@Component({
  selector: 'app-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.scss']
})
export class FacultyComponent implements OnInit {

  dataEndpoint: string;

  // use for library
  pickedProperties: ColumnItems[];
  scrollHeight = '200px';
  exportType: string[];

  constructor() {
    this.dataEndpoint = 'https://studentmanagementapi.azurewebsites.net/api/Faculty/Get';
    this.exportType = ['excel', 'pdf', 'csv'];
  }

  ngOnInit(): void {

    // use for library
    this.pickedProperties = [
      { field: 'id', header: 'Id' },
      { field: 'facultyName', header: 'Faculty Name' },
      { field: 'trainingTime', header: 'Training Time' }
    ];
  }

}
