import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepartmentsComponent } from './departments/departments.component';

import { FacultyComponent } from './faculty.component';

const routes: Routes = [{ path: '', component: FacultyComponent },
{ path: 'departments', component: DepartmentsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacultyRoutingModule { }
