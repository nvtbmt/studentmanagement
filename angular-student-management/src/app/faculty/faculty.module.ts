import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacultyRoutingModule } from './faculty-routing.module';
import { FacultyComponent } from './faculty.component';

// custom data table library
import { DatatablelibModule } from 'datatablelib';
import { TableModule } from 'primeng-lts/table';
import { DepartmentsComponent } from './departments/departments.component';

@NgModule({
  declarations: [FacultyComponent, DepartmentsComponent],
  imports: [
    CommonModule,
    FacultyRoutingModule,
    DatatablelibModule,
    TableModule,
  ]
})
export class FacultyModule { }
