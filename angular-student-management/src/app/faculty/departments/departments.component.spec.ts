import { HttpClient } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TableModule } from 'primeng-lts/table';
import { of } from 'rxjs';

import { DepartmentsComponent } from './departments.component';

describe('DepartmentsComponent', () => {
  let component: DepartmentsComponent;
  let fixture: ComponentFixture<DepartmentsComponent>;
  const http = jasmine.createSpyObj('HttpClient', ['get']);
  const mockData = [
    { id: 'id1', departmentName: 'dName1', facultyName: 'fName1' },
    { id: 'id3', departmentName: 'dName3', facultyName: 'fName2' },
    { id: 'id2', departmentName: 'dName2', facultyName: 'fName2' }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TableModule ],
      declarations: [ DepartmentsComponent ],
      providers: [
        { provide: HttpClient, useValue: http }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    http.get.and.returnValue(of(mockData));
    fixture = TestBed.createComponent(DepartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get data', () => {
    spyOn(component, 'setRowGroupMetaData');
    http.get.and.returnValue(of(mockData));
    component.ngOnInit();
    expect(component.departments).toEqual(mockData);
    expect(component.setRowGroupMetaData).toHaveBeenCalled();
  });

  it('should set RowGroupMetaData', () => {
    component.departments = mockData;
    const meta = {
      fName1: { index: 0, size: 1},
      fName2: { index: 1, size: 2}
    };
    component.setRowGroupMetaData();
    expect(component.rowGroupMetadata).toEqual(meta);
  });
});
