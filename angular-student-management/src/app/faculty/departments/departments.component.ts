import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss']
})
export class DepartmentsComponent implements OnInit {

  departments: Department[];
  rowGroupMetadata: any;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get<Department[]>('https://studentmanagementapi.azurewebsites.net/api/Department/Get')
      .subscribe((data) => {
        this.departments = data;
        this.departments.sort((a: Department, b: Department) => {
          return a.facultyName > b.facultyName ? 1 : (a.facultyName < b.facultyName ? -1 : 0);
        });
        this.setRowGroupMetaData();
      });
  }

  setRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.departments) {
      for (let i = 0; i < this.departments.length; i++) {
        const rowData = this.departments[i];
        const facultyName = rowData.facultyName;
        if (i === 0) {
          this.rowGroupMetadata[facultyName] = { index: 0, size: 1 };
        }
        else {
          const previousRowData = this.departments[i - 1];
          const previousRowGroup = previousRowData.facultyName;
          if (facultyName === previousRowGroup){
            this.rowGroupMetadata[facultyName].size++;
          }
          else{
            this.rowGroupMetadata[facultyName] = { index: i, size: 1 };
          }
        }
      }
    }
  }

}

interface Department{
  id: string;
  departmentName: string;
  facultyName: string;
}
