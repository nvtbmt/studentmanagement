import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentCreationComponent } from './student-creation/student-creation.component';
import { StudentDetailComponent } from './student-detail/student-detail.component';
import { StudentsComponent } from './students.component';

const routes: Routes = [
  { path: '', component: StudentsComponent },
  { path: 'addstudent', component: StudentCreationComponent },
  { path: 'editstudent/:studentId', component: StudentDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentsRoutingModule { }
