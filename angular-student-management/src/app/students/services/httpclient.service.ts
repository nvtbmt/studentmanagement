import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Faculty } from '../interfaces/faculty.model';
import { Student } from '../interfaces/student.model';
import { keepTimeFromTimezoneOffset } from 'src/app/utilities/datetime';

@Injectable({
  providedIn: 'root',
})
export class HttpClientService {
  baseUrl = 'https://studentmanagementapi.azurewebsites.net/api/';
  students: Student[];
  faculties: Faculty[];
  strGenders = ['Female', 'Male'];
  strGraduateds = ['Graduated', 'Undergraduate'];
  firstNames: string[];
  facultyNames: string[];

  constructor(private http: HttpClient) { }

  getFaculties() {
    return this.http.get<Faculty[]>(this.baseUrl + 'Faculty/Get')
      .pipe(
        retry(3),
        catchError(this.handleError<Faculty[]>('get faculty', []))
      );
  }

  public getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(this.baseUrl + 'Student/Get').pipe(
      map((students) => {
        students.forEach((element) => {
          element.dateOfBirth = new Date(element.dateOfBirth);
          element.enrollDate = new Date(element.enrollDate);
          element.finishDate = new Date(element.finishDate);
          element.strGender = element.gender ? this.strGenders[0] : this.strGenders[1];
          element.strGraduated = element.graduated ? this.strGraduateds[0] : this.strGraduateds[1];
        });
        return students;
      }),
      retry(3),
      catchError(this.handleError<Student[]>('Get students', []))
    );
  }

  setBoolGender(student: Student): Student {
    student.gender = student.strGender === 'Female' ? true : false;
    return student;
  }

  setBoolGraduate(student: Student): Student {
    student.graduated = student.strGraduated === this.strGraduateds[0] ? true : false;
    return student;
  }

  // setFacultyId(student: Student): Student {
  //   if (this.faculties !== undefined) {
  //     this.faculties.forEach((faculty) => {
  //       if (student.facultyName === faculty.facultyName) {
  //         student.facultyId = faculty.id;
  //       }
  //     });
  //   }

  //   return student;
  // }

  public putStudent(student: Student) {
    this.setBoolGender(student);
    this.setBoolGraduate(student);
    // this.setFacultyId(student);
    student.dateOfBirth = keepTimeFromTimezoneOffset(student.dateOfBirth);
    return this.http
      .put(this.baseUrl + 'Student/Put', student)
      .pipe(retry(3), catchError(this.handleError<Student>('put student')));
  }

  public postStudent(student: Student): Observable<Student> {
    this.setBoolGender(student);
    this.setBoolGraduate(student);
    // this.setFacultyId(student);
    student.dateOfBirth = keepTimeFromTimezoneOffset(student.dateOfBirth);
    return this.http
      .post<Student>(this.baseUrl + 'Student/Post', student)
      .pipe(retry(3), catchError(this.handleError<Student>('Post student')));
  }

  public deleteStudents(students: Student[]): Observable<Student[]> {
    let param = '?';
    students.forEach((student) => {
      param += 'ids=' + student.id + '&';
    });
    param = param.slice(0, -1);

    return this.http.delete<Student[]>(this.baseUrl + 'Student/Delete' + param)
      .pipe(
        retry(3),
        catchError(this.handleError<Student[]>('Delete student'))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} error: ${error.message}`);
      return throwError({
        message: error.message,
        object: result
      });
    };
  }

  getFirstNamesFromStudents(students: Student[]): string[]{
    this.firstNames = [];
    const flag = [];
    students.forEach((student) => {
      const firstName = student.firstName;
      if (!flag[firstName]) {
        flag[firstName] = true;
        this.firstNames.push(firstName);
      }
    });

    return this.firstNames;
  }
}
