import { HttpClient, HttpClientModule, HttpResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { HttpClientService } from './httpclient.service';
import { Faculty } from '../interfaces/faculty.model';
import { of, throwError } from 'rxjs';
import { Student } from '../interfaces/student.model';

describe('HttpclientService', () => {
  let service: HttpClientService;
  const http = jasmine.createSpyObj('HttpClient', ['get']);
  let httpTestingController: HttpTestingController;
  const mockStudents = [{
    id: 'Id',
    firstName: 'first name',
    lastName: 'last name',
    dateOfBirth: new Date(),
    gender: false,
    strGender: 'Male',
    facultyId: 'fId',
    facultyName: 'fName',
    enrollDate: new Date(),
    finishDate: new Date(),
    graduated: false,
    strGraduated: 'Undergraduate'
  }];
  const mockFaculty = [
    {id: 'id', facultyName: 'IT', trainingTime: 4}
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpClientService]
    });
    service = TestBed.inject(HttpClientService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getFaculties function should return expected values', () => {
    service.getFaculties().subscribe(
      res => expect(res).toEqual(mockFaculty)
    );
  });

  it('getFaculties function should return an error when the server return a 404 status', () => {
    service.getFaculties().subscribe(
      res => fail('expected an error'),
      error => expect(error.message).toEqual('test 404 error')
    );
  });

  it('getStudents function should return expected values', () => {
    service.getStudents().subscribe(
      res => expect(res).toEqual(mockStudents)
    );
  });

  it('getStudents function should return an error when the server return a 404 status', () => {
    service.getStudents().subscribe(
      res => fail('expected an error'),
      error => expect(error.message).toEqual('test 404 error')
    );
  });

  // post
  it('should post a student and return it', () => {
    service.postStudent(mockStudents[0]).subscribe(
      res => expect(res).toEqual(mockStudents[0], 'should return the student')
    );

    // should have made one request to POST
    const req = httpTestingController.expectOne(service.baseUrl + 'Student/Post');
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(mockStudents[0]);
  });

  it('should return error with a student in body if error', () => {
    service.postStudent(mockStudents[0]).subscribe(
      res => expect(res).toEqual(mockStudents[0], 'should return the student')
    );

    const req = httpTestingController.expectOne(service.baseUrl + 'Student/Post');

    req.error(null);
    expect(req.request.body).toEqual(mockStudents[0]);
  });

  // put
  it('should put a student and return it', () => {
    service.putStudent(mockStudents[0]).subscribe(
      res => expect(res).toEqual(mockStudents[0], 'should return the student')
    );

    // should have made one request to PUT
    const req = httpTestingController.expectOne(service.baseUrl + 'Student/Put');
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(mockStudents[0]);
  });

  it('should return error with a student in body if error', () => {
    service.putStudent(mockStudents[0]).subscribe(
      res => expect(res).toEqual(mockStudents[0], 'should return the student')
    );

    const req = httpTestingController.expectOne(service.baseUrl + 'Student/Put');

    req.error(null);
    expect(req.request.body).toEqual(mockStudents[0]);
  });

  // delete
  it('should delete an array of students and return it', () => {
    const students: Student[] = mockStudents;

    service.deleteStudents(students).subscribe(
      res => expect(res).toEqual(students, 'should return an array of students')
    );

    // should have made one request to DELETE
    const req = httpTestingController.expectOne(service.baseUrl + 'Student/Delete?ids=Id');
    expect(req.request.method).toEqual('DELETE');
  });

  it('should return error with an array of students in body if error', () => {
    service.deleteStudents(mockStudents).subscribe(
      res => expect(res).toEqual(mockStudents, 'should return an array of students')
    );

    const req = httpTestingController.expectOne(service.baseUrl + 'Student/Delete?ids=Id');

    req.error(null);
  });

  it('setBoolGender function should set gender value: bool corresponding', () => {
    service.setBoolGender(mockStudents[0]);
    expect(mockStudents[0].gender).toBe(false);
  });

  it('should get firstNames when call getFirstNamesFromStudents', () => {
    const firstNames = ['first name'];
    const res = service.getFirstNamesFromStudents(mockStudents);
    expect(res).toEqual(firstNames);
  });
});
