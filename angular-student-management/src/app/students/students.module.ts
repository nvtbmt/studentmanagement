import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';
import { ContextMenuModule } from 'primeng/contextmenu';
import { MultiSelectModule } from 'primeng/multiselect';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
// import { ConfirmPopupModule } from "primeng/confirmpopup";
import { ConfirmationService, MessageService } from 'primeng/api';
import { TableModule, SortableColumn, SortIcon } from 'primeng/table';

import { StudentsComponent } from './students.component';
import { StudentsRoutingModule } from './students-routing.module';
import { StudentDetailComponent } from './student-detail/student-detail.component';
import { StudentCreationComponent } from './student-creation/student-creation.component';

@NgModule({
  declarations: [StudentsComponent, StudentDetailComponent, StudentCreationComponent],
  imports: [
    CommonModule,
    StudentsRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    TableModule,
    ButtonModule,
    // SortableColumn,
    MultiSelectModule,
    ToastModule,
    // ConfirmPopupModule,
    ToastModule,
    DropdownModule,
    ContextMenuModule,
    DialogModule,
    RadioButtonModule,
    KeyFilterModule,
    ConfirmDialogModule,
    InputTextModule,
    CalendarModule
  ],
  exports: [
    // MatFormFieldModule,
    // MatInputModule,
  ],
  providers: [MessageService,
    ConfirmationService]
})
export class StudentsModule { }
