import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { ConfirmationService, MessageService, MenuItem, SelectItem } from 'primeng/api';

import { Faculty } from './interfaces/faculty.model';
import { HttpClientService } from './services/httpclient.service';
import { Student, studentsIsSame } from './interfaces/student.model';

// declare const require: any;
// const jsPDF = require('jspdf');
// require('jspdf-autotable');
import { jsPDF } from 'jspdf';
import * as autoTable from 'jspdf-autotable';
import { Table } from 'primeng-lts/table';
import { FilterUtils } from 'primeng/utils';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss'],
})
export class StudentsComponent implements OnInit {
  student: Student;
  students: Student[] = [];

  columns: any[];
  pickedColumns: any[];
  first = 0;

  firstNameOptions: SelectItem[] = [];
  strGenderOptions: SelectItem[] = [];
  strGraduatedOptions: SelectItem[] = [];
  facultyOptions: SelectItem[] = [];

  selectedStudent: Student;
  selectedStudents: Student[];
  facultyOfSelectedStudent: Faculty;
  contextActions: MenuItem[];
  displayDialog: boolean;
  isAddNewStudent: boolean;

  studentForm: FormGroup;

  clonedStudents: { [id: string]: Student } = {};
  @ViewChild('dt') dt: Table;

  constructor(
    private http: HttpClientService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private formBuilder: FormBuilder
  ){ }

  ngOnInit(): void {
    this.getStudent();

    this.columns = [
      { field: 'firstName', header: 'First Name' },
      { field: 'lastName', header: 'Last Name' },
      { field: 'dateOfBirth', header: 'Date of Birth' },
      { field: 'strGender', header: 'Gender' },
      { field: 'facultyName', header: 'Faculty' },
      { field: 'strGraduated', header: 'Status' },
      { field: 'enrollDate', header: 'Enroll Date' },
      { field: 'finishDate', header: 'Finish Date' },
    ];

    this.pickedColumns = this.columns;

    this.contextActions = [
      {
        label: 'Delete',
        icon: 'pi pi-times',
        command: (event) => this.deleteStudent(this.selectedStudent),
      },
    ];

    this.studentForm = this.formBuilder.group({
      id: new FormControl(''),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      dateOfBirth: new FormControl(null, Validators.required),
      strGender: new FormControl('', Validators.required),
      faculty: new FormControl(null, Validators.required),
      enrollDate: new FormControl(new Date(), Validators.required)
    });

    // tslint:disable:no-string-literal
    FilterUtils['all'] = () => true;
    FilterUtils['dateEquals'] = (value: Date, filter: Date) => {
      return value.getTime() === filter.getTime();
    };
    FilterUtils['containMonthYear'] = (value: Date, event: any) => {
      return event.month === (value.getMonth() + 1) && event.year === value.getFullYear();
    };
  }

  filterFaculty(faculties: Faculty[]){
    const values = [];
    faculties.forEach(faculty => {
      values.push(faculty.facultyName);
    });
    this.dt.filter(values, 'facultyName', 'in');
  }

  getStudent = () => {
    this.http.getFaculties()
      .subscribe((faculties) => {
        this.http.faculties = faculties as Faculty[];
        this.http.faculties.forEach((faculty) => {
          this.facultyOptions.push({
            label: faculty.facultyName,
            value: faculty
          });
        });
      });

    this.http.getStudents()
      .subscribe((students) => {
        this.http.students = students as Student[];
        this.http.getFirstNamesFromStudents(this.http.students);
        this.students = this.http.students;
        this.http.firstNames.forEach((firstName) => {
          this.firstNameOptions.push({
            label: firstName,
            value: firstName
          });
        });
      });

    this.http.strGenders.forEach((strGender) => {
      this.strGenderOptions.push({
        label: strGender,
        value: strGender
      });
    });

    this.http.strGraduateds.forEach((strGraduated) => {
      this.strGraduatedOptions.push({
        label: strGraduated,
        value: strGraduated
      });
    });
  }

  deleteStudent(student?: Student) {
    if (student) {
      this.selectedStudents = [student];
    }

    if (
      this.selectedStudents == null ||
      this.selectedStudents.length === 0
    ) {
      this.messageService.add({
        severity: 'info',
        summary: 'No student has selected',
        detail: 'Select at least one student to delete!',
      });
      return;
    }

    this.confirmationService.confirm({
      header: 'Delete students',
      message: 'Are you sure that you want to DELETE this?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.http.deleteStudents(this.selectedStudents)
        .subscribe(() => {
          this.getStudent();
          let detail = '';
          this.selectedStudents.forEach((selectedStudent) => {
            detail += selectedStudent.firstName + ' ' + selectedStudent.lastName + ', \n';
          });
          detail = detail.slice(0, -1);
          this.messageService.add({
            severity: 'success',
            summary: 'Student Deleted Successfully',
            detail
          });
        });
      },
      reject: () => {},
    });
  }

  // Row editing
  onRowEditInit(student: Student) {
    // if (this.clonedStudents[student.id] !== undefined){
    //   return;
    // }
    this.facultyOfSelectedStudent = this.http.faculties.find(x => x.id === student.facultyId);
    this.clonedStudents[student.id] = { ...student };
  }

  onRowEditSave(student: Student) {
    student.facultyId = this.facultyOfSelectedStudent.id;
    student.facultyName = this.facultyOfSelectedStudent.facultyName;
    if (studentsIsSame(student, this.clonedStudents[student.id])){
      return;
    }
    this.http.putStudent(student)
    .subscribe(() => {
      this.messageService.add({
        severity: 'success',
        summary: 'Student Edited Successful',
        detail:
          student.firstName + ' ' + student.lastName + ', ' + student.strGender,
      });
      delete this.clonedStudents[student.id];
    });
  }

  onRowEditCancel(student: Student, index: number) {
    if (this.clonedStudents[student.id] !== undefined && studentsIsSame(student, this.clonedStudents[student.id])){
      this.students[index] = this.clonedStudents[student.id];
      delete this.clonedStudents[student.id];
      return;
    }
    this.confirmationService.confirm({
      header: 'Cancel',
      message: 'Changes will be unsave. Do you want to Cancel?',
      icon: 'pi pi-info',
      accept: () => {
        this.students[index] = this.clonedStudents[student.id];
        delete this.clonedStudents[student.id];
      }
    });
  }

  // crud
  addStudent() {
    this.isAddNewStudent = true;
    this.student = {
      id: '',
      firstName: '',
      lastName: '',
      dateOfBirth: null,
      gender: false,
      strGender: 'Male',
      graduated: false,
      strGraduated: 'Undergraduate',
      facultyId: '',
      facultyName: '',
      enrollDate: null,
      finishDate: null,
    };
    this.displayDialog = true;
  }

  crudSave(student: Student) {
    if (this.isAddNewStudent) {
      this.http.postStudent(student)
      .subscribe(() => {
        this.messageService.add({
          severity: 'success',
          summary: 'Student Added Successfully',
          detail:
            student.firstName +
            ' ' +
            student.lastName +
            ', ' +
            student.strGender,
        });
        this.isAddNewStudent = false;
        this.getStudent();
      });
    } else {
      this.onRowEditSave(student);
    }

    this.displayDialog = false;
  }

  onShow() {
    if (this.isAddNewStudent) {
      this.studentForm = this.formBuilder.group({
        id: new FormControl(''),
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        dateOfBirth: new FormControl(null, Validators.required),
        strGender: new FormControl('', Validators.required),
        faculty: new FormControl(null, Validators.required),
        enrollDate: new FormControl(new Date(), Validators.required)
      });
    } else {
      this.studentForm = this.formBuilder.group({
        id: this.student.id,
        firstName: new FormControl(this.student.firstName, Validators.required),
        lastName: new FormControl(this.student.lastName, Validators.required),
        dateOfBirth: new FormControl(this.student.dateOfBirth, Validators.required),
        strGender: new FormControl(this.student.strGender, Validators.required),
        faculty: new FormControl(
          this.http.faculties.find(x => x.id === this.student.facultyId),
          Validators.required
        ),
        enrollDate: new FormControl(this.student.enrollDate, Validators.required)
      });
    }
  }

  public get firstName() {
    return this.studentForm.get('firstName');
  }
  public get lastName() {
    return this.studentForm.get('lastName');
  }
  public get dateOfBirth() {
    return this.studentForm.get('dateOfBirth');
  }
  public get strGender() {
    return this.studentForm.get('strGender');
  }
  public get faculty() {
    return this.studentForm.get('faculty');
  }
  public get enrollDate() {
    return this.studentForm.get('enrollDate');
  }

  // toggle column
  checkColumn(header: string) {
    let isExist = false;
    this.pickedColumns.forEach((column) => {
      if (column.header === header) {
        isExist = true;
      }
    });
    return isExist;
  }

  outOfDate(graduated: boolean, finishDate: Date): boolean{
    const current = new Date();
    if (!graduated && current > finishDate) {
      return true;
    }
    return false;
  }

  // exportPdf() {
  //   const doc = new jsPDF();
  //   doc.autoTable();
  //   doc.save('a4.pdf');
  // }

  exportExcel() {
    import('xlsx').then((xlsx) => {
      const worksheet = xlsx.utils.json_to_sheet(this.students);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(excelBuffer, 'tableName');
    });
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then((FileSaver) => {
      const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      const EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE,
      });
      FileSaver.saveAs(
        data,
        fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
      );
    });
  }
}
