import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';

import { Student } from '../interfaces/student.model';
import { HttpClientService } from '../services/httpclient.service';
import { StudentDetailComponent } from './student-detail.component';

describe('StudentDetailComponent', () => {
  let component: StudentDetailComponent;
  let fixture: ComponentFixture<StudentDetailComponent>;
  const fakeActivatedRoute = { snapshot: { paramMap: { get: (name) => 'id' } } } as ActivatedRoute;
  let thisStudent: Student;
  let location: Location;
  const service = jasmine.createSpyObj('HttpClientService',
    ['getFaculties', 'getStudents', 'putStudent']
  );
  const mockFaculties = [
    { id: 'is', facultyName: 'Faculty of Information Systems', trainingTime: 4.5 },
    { id: 'cs', facultyName: 'Faculty of Computer Science', trainingTime: 4 }
  ];
  const mockStudents = [
    {
      id: 'id',
      firstName: 'fName',
      lastName: 'lName',
      dateOfBirth: new Date(),
      gender: false,
      strGender: 'Male',
      facultyId: 'is',
      facultyName: 'Faculty of Information Systems',
      enrollDate: new Date(),
      finishDate: new Date(),
      graduated: false,
      strGraduated: ''
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentDetailComponent ],
      imports: [ FormsModule, HttpClientTestingModule, RouterTestingModule, ReactiveFormsModule,
        ToastModule, DropdownModule, CalendarModule, RadioButtonModule
      ],
      providers: [ FormBuilder, MessageService,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
        { provide: HttpClientService, useValue: service }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    service.students = mockStudents;
    service.faculties = mockFaculties;
    thisStudent = mockStudents[0];
    fixture = TestBed.createComponent(StudentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    location = TestBed.inject(Location);
  });

  afterEach(() => {
    service.putStudent.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get correct student which has Id from url parameter', () => {
    expect(thisStudent).toEqual(component.student);
  });

  it('should get students if it is undefined', () => {
    service.students = undefined;
    service.getStudents.and.returnValue(of(mockStudents));
    component.ngOnInit();
    expect(service.getStudents).toHaveBeenCalled();
  });

  it('should validate form', () => {
    expect(component.studentForm.valid).toBeTruthy();
    const expectedControlStatus = component.firstName.valid && component.lastName.valid && component.strGender.valid
      && component.dateOfBirth.valid && component.faculty.valid && component.enrollDate.valid;
    expect(expectedControlStatus).toBeTruthy();
    component.studentForm.reset();
    expect(component.studentForm.valid).toBeFalsy();
  });

  it('should setup faculty options', () => {
    service.getFaculties.and.returnValue(of(mockFaculties));
    spyOn(component, 'setfacultyOptions');
    service.faculties = undefined;
    component.ngOnInit();
    expect(component.setfacultyOptions).toHaveBeenCalled();
    expect(service.getFaculties).toHaveBeenCalled();
  });

  it('shouldn`t post student if studentForm is invalid', () => {
    component.studentForm.setErrors({valid: false});
    component.onSubmit(component.studentForm.value);
    expect(service.putStudent).not.toHaveBeenCalled();
  });

  it('should post student when submit the form', () => {
    service.putStudent.and.returnValue(of({}));
    component.onSubmit(component.studentForm.value);
    expect(service.putStudent).toHaveBeenCalled();
  });

  it('html form should have correct values from studentForm', () => {
    const firstNameTxt = fixture.debugElement.query(By.css('#firstName')).nativeElement.value;
    const lastNameTxt = fixture.debugElement.query(By.css('#lastName')).nativeElement.value;
    const maleChecked = fixture.debugElement.query(By.css('#male')).nativeElement.checked;
    const femaleChecked = fixture.debugElement.query(By.css('#female')).nativeElement.checked;
    const dobTxt = fixture.debugElement.query(By.css('#dateOfBirth .ui-inputtext')).nativeElement.value;
    const facultyTxt = fixture.debugElement.query(By.css('#faculty .ui-dropdown-label')).nativeElement.textContent;

    const dob = (component.studentForm.get('dateOfBirth').value) as Date;
    const dobDate = dob.getDate();
    const dobMonth = dob.getMonth() + 1;
    const convertedDob = (dobMonth >= 10 ? dobMonth : ('0' + dobMonth))
      + '/' + (dobDate >= 10 ? dobDate : ('0' + dobDate)) + '/' + dob.getFullYear();

    expect(firstNameTxt).toEqual(component.firstName.value);
    expect(lastNameTxt).toEqual(component.lastName.value);
    expect(maleChecked).toEqual(component.strGender.value[0] === 'Male');
    expect(femaleChecked).toEqual(component.strGender.value[0] === 'Female');
    expect(facultyTxt).toEqual(component.faculty.value.facultyName);
    expect(dobTxt).toEqual(convertedDob);
  });

  it('should call location.back', () => {
    spyOn(location, 'back');
    component.back();
    expect(location.back).toHaveBeenCalled();
  });
});
