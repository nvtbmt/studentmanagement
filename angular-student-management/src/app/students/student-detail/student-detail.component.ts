import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { MessageService, SelectItem } from 'primeng/api';

import { Faculty } from '../interfaces/faculty.model';
import { Student } from '../interfaces/student.model';
import { HttpClientService } from '../services/httpclient.service';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.scss']
})
export class StudentDetailComponent implements OnInit {

  studentForm: FormGroup;
  student: Student;
  facultyOptions: SelectItem[];

  constructor(
    private http: HttpClientService,
    private activatedroute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private location: Location,
    private messageService: MessageService
  ) {
    this.facultyOptions = [];
    this.studentForm = formBuilder.group({
      id: '',
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dateOfBirth: [null, Validators.required],
      strGender: ['', Validators.required],
      faculty: ['', Validators.required],
      enrollDate: [new Date(), Validators.required]
    });
  }

  ngOnInit(): void {
    const studentId = this.activatedroute.snapshot.paramMap.get('studentId');
    if (this.http.students){
      this.student = this.http.students.filter(
        (student) => student.id === studentId
      )[0];
    }
    else{
      this.http.getStudents().subscribe((students) => {
        this.http.students = students as Student[];
        this.student = this.http.students.filter(
          (student) => student.id === studentId
        )[0];
      });
    }

    if (this.http.faculties !== undefined){
      this.setfacultyOptions();
    }
    else{
      this.http.getFaculties()
        .subscribe((faculties) => {
          this.http.faculties = faculties as Faculty[];
          this.setfacultyOptions();
        });
    }

    this.studentForm = this.formBuilder.group({
      id: new FormControl(this.student.id),
      firstName: new FormControl(this.student.firstName, Validators.required),
      lastName: new FormControl(this.student.lastName, Validators.required),
      dateOfBirth: new FormControl(this.student.dateOfBirth, Validators.required),
      strGender: new FormControl(this.student.gender ? ['Female'] : ['Male'], Validators.required),
      faculty: new FormControl(
        this.http.faculties.find(x => x.id === this.student.facultyId),
        Validators.required
      ),
      enrollDate: new FormControl(this.student.enrollDate, Validators.required)
    });
  }

  setfacultyOptions(){
    this.http.faculties.forEach((faculty) => {
      this.facultyOptions.push({
        label: faculty.facultyName,
        value: faculty,
      });
    });
  }

  onSubmit(data: Student) {
    if (!this.studentForm.valid){
      return;
    }
    data.facultyId = this.studentForm.get('faculty').value.id;
    data.facultyName = this.studentForm.get('faculty').value.facultyName;
    this.http.putStudent(data)
    .subscribe(() => {
      this.messageService.add({
        severity: 'success',
        summary: 'Student Edited Successfully',
        detail: data.firstName + ' ' + data.lastName + ', ' + data.strGender,
      });
      this.back();
    });
  }

  back(){
    this.location.back();
  }

  public get firstName() {
    return this.studentForm.get('firstName');
  }
  public get lastName() {
    return this.studentForm.get('lastName');
  }
  public get dateOfBirth() {
    return this.studentForm.get('dateOfBirth');
  }
  public get strGender() {
    return this.studentForm.get('strGender');
  }
  public get faculty() {
    return this.studentForm.get('faculty');
  }
  public get enrollDate() {
    return this.studentForm.get('enrollDate');
  }
}
