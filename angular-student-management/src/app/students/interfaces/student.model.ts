export interface Student {
  id: string;
  firstName: string;
  lastName: string;
  dateOfBirth: Date;
  gender: boolean;
  strGender: string;
  facultyId: string;
  facultyName: string;
  graduated: boolean;
  strGraduated: string;
  enrollDate: Date;
  finishDate: Date;
}

export function studentsIsSame(student1: Student, student2: Student): boolean {
    return JSON.stringify(student1) === JSON.stringify(student2);
}
