import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';

import { HttpClientService } from 'src/app/students/services/httpclient.service';
import { StudentCreationComponent } from './student-creation.component';

describe('StudentCreationComponent', () => {
  let component: StudentCreationComponent;
  let fixture: ComponentFixture<StudentCreationComponent>;
  const service = jasmine.createSpyObj('HttpClientService',
    ['getFaculties', 'getStudents', 'postStudent']
  );
  const mockFaculties = [
    { id: 'is', facultyName: 'Faculty of Information Systems', trainingTime: 4.5 },
    { id: 'cs', facultyName: 'Faculty of Computer Science', trainingTime: 4 }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentCreationComponent ],
      imports: [
        HttpClientTestingModule, ReactiveFormsModule, BrowserAnimationsModule,
        ToastModule, DropdownModule, CalendarModule, RadioButtonModule
      ],
      providers: [
        { provide: HttpClientService, useValue: service },
        FormBuilder, MessageService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    service.getFaculties.and.returnValue(of(mockFaculties));
    fixture = TestBed.createComponent(StudentCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    service.postStudent.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should setup faculty options', () => {
    spyOn(component, 'setfacultyOptions');
    service.faculties = undefined;
    component.ngOnInit();
    expect(component.setfacultyOptions).toHaveBeenCalled();
    expect(service.getFaculties).toHaveBeenCalled();
  });

  it('should validate form', () => {
    expect(component.studentForm.valid).toBeFalsy();
    const expectedControlStatus = component.firstName.invalid && component.lastName.invalid && component.strGender.invalid
      && component.dateOfBirth.invalid && component.faculty.invalid && component.enrollDate.valid;
    expect(expectedControlStatus).toBeTruthy();
    component.firstName.setValue('first name');
    component.lastName.setValue('last name');
    component.strGender.setValue(['Male']);
    component.faculty.setValue(service.faculties[0]);
    component.dateOfBirth.setValue(new Date());
    expect(component.studentForm.valid).toBeTruthy();
  });

  it('html form inputs value should be values of formbuilder', () => {
    const firstNameElement = fixture.debugElement.query(By.css('#firstName'));
    const lastNameElement = fixture.debugElement.query(By.css('#lastName'));
    const maleElement = fixture.debugElement.query(By.css('#male'));
    const femaleElement = fixture.debugElement.query(By.css('#female'));
    expect(firstNameElement.nativeElement.value).toBe('');
    expect(lastNameElement.nativeElement.value).toBe('');
    expect(maleElement.nativeElement.checked).toBeFalse();
    expect(femaleElement.nativeElement.checked).toBeFalse();
  });

  it('shouldn`t post student if studentForm is invalid', () => {
    component.studentForm.setErrors({valid: false});
    component.onSubmit(component.studentForm.value);
    expect(service.postStudent).not.toHaveBeenCalled();
  });

  it('should post student when submit the form', () => {
    component.firstName.setValue('first name');
    component.lastName.setValue('last name');
    component.strGender.setValue(['Male']);
    component.faculty.setValue(service.faculties[0]);
    component.dateOfBirth.setValue(new Date());
    service.postStudent.and.returnValue(of({}));
    component.onSubmit(component.studentForm.value);
    expect(service.postStudent).toHaveBeenCalled();
  });
});
