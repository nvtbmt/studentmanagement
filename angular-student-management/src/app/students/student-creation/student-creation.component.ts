import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { MessageService, SelectItem } from 'primeng/api';

import { Faculty } from '../interfaces/faculty.model';
import { Student } from '../interfaces/student.model';
import { HttpClientService } from '../services/httpclient.service';

@Component({
  selector: 'app-student-creation',
  templateUrl: './student-creation.component.html',
  styleUrls: ['./student-creation.component.scss']
})
export class StudentCreationComponent implements OnInit {

  studentForm: FormGroup;
  newStudent: Student;
  facultyOptions: SelectItem[];

  constructor(
    private http: HttpClientService,
    formBuilder: FormBuilder,
    private messageService: MessageService
  ) {
    this.facultyOptions = [];
    this.studentForm = formBuilder.group({
      id: new FormControl(''),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      dateOfBirth: new FormControl(null, Validators.required),
      strGender: new FormControl('', Validators.required),
      faculty: new FormControl(null, Validators.required),
      enrollDate: new FormControl(new Date(), Validators.required)
    });
  }

  ngOnInit(): void {
    if (this.http.faculties !== undefined){
      this.setfacultyOptions();
    }
    else{
      this.http.getFaculties()
        .subscribe((faculties) => {
          this.http.faculties = faculties as Faculty[];
          this.setfacultyOptions();
        });
    }
  }

  setfacultyOptions(){
    this.http.faculties.forEach((faculty) => {
      this.facultyOptions.push({
        label: faculty.facultyName,
        value: faculty,
      });
    });
  }

  onSubmit(data: Student) {
    if (!this.studentForm.valid){
      return;
    }
    data.facultyId = this.studentForm.get('faculty').value.id;
    data.facultyName = this.studentForm.get('faculty').value.facultyName;
    this.newStudent = data;
    this.http.postStudent(data)
    .subscribe(() => {
      this.messageService.add({
        severity: 'info',
        summary: 'Student Added Successfully',
        detail: data.firstName + ' ' + data.lastName + ', ' + data.strGender,
      });
    });
  }

  public get firstName() {
    return this.studentForm.get('firstName');
  }
  public get lastName() {
    return this.studentForm.get('lastName');
  }
  public get dateOfBirth() {
    return this.studentForm.get('dateOfBirth');
  }
  public get strGender() {
    return this.studentForm.get('strGender');
  }
  public get faculty() {
    return this.studentForm.get('faculty');
  }
  public get enrollDate() {
    return this.studentForm.get('enrollDate');
  }
}
