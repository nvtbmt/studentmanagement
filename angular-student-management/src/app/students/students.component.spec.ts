import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng-lts/table';
import { ToastModule } from 'primeng-lts/toast';
import { DialogModule } from 'primeng-lts/dialog';
import { CalendarModule } from 'primeng-lts/calendar';
import { DropdownModule } from 'primeng-lts/dropdown';
import { MultiSelectModule } from 'primeng-lts/multiselect';
import { ContextMenuModule } from 'primeng-lts/contextmenu';
import { RadioButtonModule } from 'primeng-lts/radiobutton';
import { ConfirmDialogModule } from 'primeng-lts/confirmdialog';
import { Confirmation, ConfirmationService, MessageService } from 'primeng-lts/api';

import { StudentsComponent } from './students.component';
import { HttpClientService } from './services/httpclient.service';
import { FilterUtils } from 'primeng-lts/utils';

describe('StudentsComponent', () => {
  let component: StudentsComponent;
  let fixture: ComponentFixture<StudentsComponent>;
  let messageService: MessageService;
  let confirmationService: ConfirmationService;
  const service = jasmine.createSpyObj('HttpClientService',
    ['getFaculties', 'getStudents', 'getFirstNamesFromStudents', 'postStudent', 'putStudent', 'deleteStudents']
  );
  const mockFaculties = [
    { id: 'is', facultyName: 'Faculty of Information Systems', trainingTime: 4.5 },
    { id: 'cs', facultyName: 'Faculty of Computer Science', trainingTime: 4 }
  ];
  const facultyOptions = [
    { label: 'Faculty of Information Systems', value: mockFaculties[0] },
    { label: 'Faculty of Computer Science', value: mockFaculties[1] }
  ];
  const mockStudents = [
    {
      id: 'id',
      firstName: 'fName',
      lastName: 'lName',
      dateOfBirth: new Date(),
      gender: false,
      strGender: 'Male',
      facultyId: 'is',
      facultyName: 'Faculty of Information Systems',
      enrollDate: new Date(),
      finishDate: new Date(),
      graduated: false,
      strGraduated: ''
    }
  ];
  const firstNameOptions = [
    { label: 'fName', value: 'fName' }
  ];
  let student;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsComponent ],
      imports: [ HttpClientTestingModule, RouterTestingModule, FormsModule, ReactiveFormsModule,
        DialogModule, ToastModule, TableModule, ContextMenuModule, ConfirmDialogModule, MultiSelectModule, CalendarModule,
        RadioButtonModule, DropdownModule, RadioButtonModule ],
      providers: [
        ConfirmationService, MessageService, FormBuilder,
        { provide: HttpClientService, useValue: service }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    service.getFaculties.and.returnValue(of(mockFaculties));
    service.getStudents.and.returnValue(of(mockStudents));
    service.firstNames = ['fName'];
    service.strGenders = ['Female', 'Male'];
    service.strGraduateds = ['Graduated', 'Undergraduate'];
    fixture = TestBed.createComponent(StudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    messageService = TestBed.inject(MessageService);
    confirmationService = TestBed.inject(ConfirmationService);
    component.students = mockStudents;
    student = {...mockStudents[0]};
    component.clonedStudents[student.id] = {...student};
    component.facultyOfSelectedStudent = {...mockFaculties[0]};
  });

  afterEach(() => {
    service.putStudent.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get data and setup data when call getStudent', () => {
    component.facultyOptions = [];
    component.firstNameOptions = [];
    component.strGenderOptions = [];
    component.strGraduatedOptions = [];
    component.getStudent();
    expect(service.faculties).toEqual(mockFaculties);
    expect(service.students).toEqual(mockStudents);
    expect(service.getFirstNamesFromStudents).toHaveBeenCalled();
    expect(component.students).toEqual(mockStudents);
    expect(component.facultyOptions).toEqual(facultyOptions);
    expect(component.firstNameOptions).toEqual(firstNameOptions);
    expect(component.strGenderOptions).toEqual([
      { label: 'Female', value: 'Female' },
      { label: 'Male', value: 'Male' }
    ]);
    expect(component.strGraduatedOptions).toEqual([
      { label: 'Graduated', value: 'Graduated' },
      { label: 'Undergraduate', value: 'Undergraduate' }
    ]);
  });

  // tslint:disable:no-string-literal
  it('should return true for filter all', () => {
    const res = FilterUtils['all']();
    expect(res).toBeTruthy();
  });

  it('should return true if two date are equals for dateEquals filter', () => {
    const res = FilterUtils['dateEquals'](new Date(), new Date());
    expect(res).toBeTruthy();
  });

  it('should return true if the date is contain month and year of the other date for containMonthYear filter', () => {
    const mockEvent = { month: (new Date()).getMonth() + 1, year: (new Date()).getFullYear() };
    const res = FilterUtils['containMonthYear'](new Date(), mockEvent);
    expect(res).toBeTruthy();
  });

  it('should call deleteStudent if contextActions pressed ', () => {
    spyOn(component, 'deleteStudent');
    component.contextActions[0].command();
    expect(component.deleteStudent).toHaveBeenCalledWith(component.selectedStudent);
  });

  it('should add student to array if delete a student', () => {
    component.deleteStudent(student);
    expect(component.selectedStudents).toEqual([student]);
  });

  it('should show message if no student selected to delete', () => {
    spyOn(messageService, 'add');
    component.selectedStudents = [];
    component.deleteStudent();
    expect(messageService.add).toHaveBeenCalled();
  });

  it('should show confirm dialog when delete students', () => {
    spyOn(confirmationService, 'confirm').and.callFake((confirmation: Confirmation) => confirmation.accept());
    service.deleteStudents.and.returnValue(of({}));
    component.selectedStudents = mockStudents;
    component.deleteStudent();
    expect(service.deleteStudents).toHaveBeenCalled();
  });

  it('should filter faculty', () => {
    spyOn(component.dt, 'filter');
    const values = [mockFaculties[0].facultyName];
    component.filterFaculty([mockFaculties[0]]);
    expect(component.dt.filter).toHaveBeenCalledWith(values, 'facultyName', 'in');
  });

  it('should initial row editing', () => {
    component.clonedStudents = {};
    component.onRowEditInit(student);
    expect(component.facultyOfSelectedStudent).toEqual(mockFaculties[0]);
    expect(component.clonedStudents[student.id] ).toEqual(student);
  });

  it('should update faculty before saving', () => {
    component.facultyOfSelectedStudent = {...mockFaculties[1]};
    service.putStudent.and.returnValue(of({}));

    component.onRowEditSave(student);
    expect(student.facultyId).toEqual(mockFaculties[1].id);
    expect(student.facultyName).toEqual(mockFaculties[1].facultyName);
  });

  it('should ignore saving if have no changed', () => {
    // console.log(student);
    // console.log(component.clonedStudents[student.id]);
    student = {...mockStudents[0]};
    component.clonedStudents[student.id] = {...student};
    component.onRowEditSave(student);
    expect(service.putStudent).not.toHaveBeenCalled();
  });

  it('should save student if have changed', () => {
    component.facultyOfSelectedStudent = {...mockFaculties[1]};
    spyOn(messageService, 'add');
    service.putStudent.and.returnValue(of({}));

    component.onRowEditSave(student);

    expect(service.putStudent).toHaveBeenCalled();
    expect(messageService.add).toHaveBeenCalled();
    expect(component.clonedStudents[student.id]).toEqual(undefined);
  });

  it('should cancel if call onRowEditCancel and have no changed', () => {
    const temp = {...student};
    spyOn(confirmationService, 'confirm');

    component.onRowEditCancel(student, 0);
    expect(component.students[0]).toEqual(temp);
    expect(component.clonedStudents[student.id]).toEqual(undefined);
    expect(confirmationService.confirm).not.toHaveBeenCalled();
  });

  it('should show confirm if call onRowEditCancel and have changed', () => {
    const temp = {...student};
    student.facultyId = mockFaculties[1].id;
    student.facultyName = mockFaculties[1].facultyName;
    spyOn(confirmationService, 'confirm').and.callFake((confirmation: Confirmation) => confirmation.accept());

    component.onRowEditCancel(student, 0);

    expect(confirmationService.confirm).toHaveBeenCalled();
    expect(component.students[0]).toEqual(temp);
    expect(component.clonedStudents[student.id]).toEqual(undefined);
  });

  it('should create empty student and display dialog when call addStudent', () => {
    component.addStudent();
    expect(component.isAddNewStudent).toBeTruthy();
    expect(component.displayDialog).toBeTruthy();
    expect(component.student).toEqual({
      id: '',
      firstName: '',
      lastName: '',
      dateOfBirth: null,
      gender: false,
      strGender: 'Male',
      graduated: false,
      strGraduated: 'Undergraduate',
      facultyId: '',
      facultyName: '',
      enrollDate: null,
      finishDate: null,
    });
  });

  it('should post student if add new student', () => {
    component.isAddNewStudent = true;
    spyOn(messageService, 'add');
    spyOn(component, 'getStudent');
    service.postStudent.and.returnValue(of({}));

    component.crudSave(student);

    expect(service.postStudent).toHaveBeenCalledWith(student);
    expect(messageService.add).toHaveBeenCalled();
    expect(component.getStudent).toHaveBeenCalled();
    expect(component.isAddNewStudent).toBeFalsy();
    expect(component.displayDialog).toBeFalsy();
  });

  it('should call onRowEditSave if crudSave called by row editing', () => {
    component.isAddNewStudent = false;
    spyOn(component, 'onRowEditSave');
    component.crudSave(student);
    expect(component.onRowEditSave).toHaveBeenCalledWith(student);
  });

  it('should setup form when dialog showed', () => {
    component.isAddNewStudent = true;
    component.onShow();
    expect(component.studentForm.valid).toBeFalsy();

    component.isAddNewStudent = false;
    component.student = student;
    component.onShow();
    expect(component.studentForm.valid).toBeTruthy();
    expect(component.faculty.value).toEqual(mockFaculties[0]);
  });

  it('should check if student is out of graduation deadline ', () => {
    const today = new Date();
    const lastYear = new Date((new Date()).getFullYear() - 1, 1);
    const nextYear = new Date((new Date()).getFullYear() + 1, 1);
    let res = component.outOfDate(false, lastYear);
    expect(res).toBeTruthy();
    res = component.outOfDate(false, nextYear) || component.outOfDate(true, lastYear);
    expect(res).toBeFalsy();
  });
});
