import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import { MenuModule } from 'primeng-lts/menu';
import { SidebarModule } from 'primeng-lts/sidebar';

import { LeftMenuComponent } from './left-menu.component';

describe('LeftMenuComponent', () => {
  let component: LeftMenuComponent;
  let fixture: ComponentFixture<LeftMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftMenuComponent ],
      imports: [MenuModule, BrowserAnimationsModule, RouterTestingModule, SidebarModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('menu should be toogle corresponding to showMenu variable', () => {
    component.showMenu = false;
    fixture.detectChanges();
    let showingMenuElement = fixture.debugElement.query(By.css('.show'));
    expect(showingMenuElement).toBeFalsy();

    component.showMenu = true;
    fixture.detectChanges();
    showingMenuElement = fixture.debugElement.query(By.css('.show'));
    expect(showingMenuElement).toBeTruthy();
  });

  it('should update showMenu', () => {
    component.show = !component.show;
    component.ngOnChanges();
    expect(component.showMenu).toEqual(component.show);
  });
});
