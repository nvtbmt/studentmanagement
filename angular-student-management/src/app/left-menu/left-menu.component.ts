import { Component, Input, OnChanges, OnInit } from '@angular/core';

import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit, OnChanges {

  @Input() show: boolean;
  showMenu: boolean;
  menuItems: MenuItem[];
  side: boolean;
  constructor() {
    this.side =  false;
    this.menuItems = [
      {
        label: 'Home Page', icon: 'pi pi-fw pi-home',
        items: [
          {label: 'Dashboard', routerLink: ['/dashboard']}
        ]
      },
      {
        label: 'Faculty',
        items: [
          {label: 'Faculties', routerLink: ['/faculties']},
          {label: 'Departments', routerLink: ['/faculties/departments']}
        ]
      },
      {
        label: 'Student',
        items: [
          {label: 'Students', icon: 'pi pi-user', routerLink: ['/students']},
          {label: 'Add Student', icon: 'pi pi-fw pi-user-plus', routerLink: ['/students/addstudent']},
        ]
      },
      {
        label: 'Activity',
        items: [
          {label: 'Activities', icon: 'pi pi-user', routerLink: ['/activities']},
        ]
      }
  ];
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void{
    this.showMenu = this.show;
  }

}
