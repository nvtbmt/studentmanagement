import { AddStudent } from '../page-objects/add-student.po';

describe('add student page', () => {
    const addStudentPage = new AddStudent();
    beforeEach(() => {
        addStudentPage.navigate();
    });

    it('expect initial form input value', () => {
        expect(addStudentPage.getInputValue('firstName')).toEqual('');
        expect(addStudentPage.getInputValue('lastName')).toEqual('');
    });

    it('editing form should be validated', () => {
        const firstName = addStudentPage.getInput('firstName');
        const lastName = addStudentPage.getInput('lastName');
        addStudentPage.sendKeyIntoInput(firstName, 'Joao');
        addStudentPage.sendKeyIntoInput(lastName, 'Felix');
        addStudentPage.clickRadioItem('male');
        addStudentPage.selectDropdownItem('#facultyName', true);

        expect(addStudentPage.isSubmitButtonDisabled()).toBeTruthy();

        addStudentPage.removeInputValue(firstName);
        expect(addStudentPage.isSubmitButtonDisabled()).toBeFalsy();
        addStudentPage.sendKeyIntoInput(firstName, 'Joao');

        addStudentPage.removeInputValue(lastName);
        expect(addStudentPage.isSubmitButtonDisabled()).toBeFalsy();
    });
});
