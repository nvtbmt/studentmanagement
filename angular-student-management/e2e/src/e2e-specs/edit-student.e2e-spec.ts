import { EditStudent } from '../page-objects/edit-student.po';

describe('edit student page', () => {
    const editStudentPage = new EditStudent();
    beforeEach(() => {
        editStudentPage.navigate();
    });

    it('editing form should be validated', () => {
        expect(editStudentPage.isSubmitButtonDisabled()).toBeFalsy();
        const firstName = editStudentPage.getInput('firstName');
        const lastName = editStudentPage.getInput('lastName');

        editStudentPage.removeInputValue(firstName);
        expect(editStudentPage.isSubmitButtonDisabled()).toBeTruthy();
        editStudentPage.sendKeyIntoInput(firstName, 'Joao');

        editStudentPage.removeInputValue(lastName);
        expect(editStudentPage.isSubmitButtonDisabled()).toBeTruthy();
        editStudentPage.sendKeyIntoInput(lastName, 'Felix');

        expect(editStudentPage.isSubmitButtonDisabled()).toBeFalsy();

        editStudentPage.clickRadioItem('male');
        editStudentPage.selectDropdownItem('#facultyName', true);

        editStudentPage.clickSubmitButton();
    });
});
