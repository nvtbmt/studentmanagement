import { Home } from '../page-objects/home.po';

describe('home page', () => {
    const homePage = new Home();
    beforeEach(() => {
        homePage.navigateToHome();
    });

    it('should have a toogle button and toogle the left menu', () => {
        expect(homePage.buttonCreated()).toBeTruthy('The button should exist');
        expect(homePage.isShowedMenuPresent()).toBeTruthy('The left menu should be show');
        homePage.clickButton();
        expect(homePage.isShowedMenuPresent()).toBeFalsy('The left menu should hide now');
    });
});
