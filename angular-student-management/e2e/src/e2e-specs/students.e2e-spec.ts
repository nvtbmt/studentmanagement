import { Student } from '../page-objects/students.po';

describe('student page', () => {
    const studentsPage = new Student();
    beforeEach(() => {
        studentsPage.navigate();
    });

    it('table should be presented', () => {
        expect(studentsPage.isTablePresent()).toBeTruthy();
    });

    it('should have table caption text', () => {
        expect(studentsPage.getTableCaptionText()).toContain('List of Students');
    });

    it('should column toggle working', () => {
        expect(studentsPage.isColumnToggleMenuPresent()).toBeTruthy('column toggle object should be presented');
        expect(studentsPage.getColumnToggleMenuItems()).toEqual(['Id', 'First Name', 'Last Name', 'Gender', 'Faculty', 'Enroll Date', 'Finish Date']);
    });

    it('should have table header', () => {
        expect(studentsPage.getTableHeader()).toEqual(['Id', 'First Name', 'Last Name', 'Gender', 'Faculty', 'Enroll Date', 'Finish Date', 'Edit', 'Delete', 'Edit directly']);
    });

    it('should show popup when clicking to addButton, form should be validated', () => {
        expect(studentsPage.isAddButtonPresent()).toBeTruthy('add button should be presented');
        expect(studentsPage.isModalDialogPresent()).toBeFalsy('dialog should be hidden, not yet');
        studentsPage.clickAddButton();
        expect(studentsPage.isModalDialogPresent()).toBeTruthy('dialog should be presented');
        expect(studentsPage.isSubmitButtonDisabled()).toBeTruthy('summit button should be disabled');
        studentsPage.inputSendKey('firstName', 'Joao');
        studentsPage.inputSendKey('lastName', 'Felix');
        studentsPage.clickRadioItem('male');
        studentsPage.selectDropdownItem('#facultyName', 0);
        setTimeout(() => {
            expect(studentsPage.isSubmitButtonDisabled()).toBeFalsy('summit button should be enabled');
            studentsPage.clickSubmitButton();
            expect(studentsPage.isModalDialogPresent()).toBeFalsy('dialog should be hidden');
        }, 1000);
    });

    it('value of cells in an specified column should equal to filter value', async () => {
        const firstNameValues = await studentsPage.getColumnFilterValues(1, 0);
        const firstNameValue = await studentsPage.getFilterValue(1, 0);
        expect(studentsPage.allArrayItemsEqualToValue(firstNameValues, firstNameValue));

        const genderValues = await studentsPage.getColumnFilterValues(3, 0);
        const genderValue = await studentsPage.getFilterValue(3, 0);
        expect(studentsPage.allArrayItemsEqualToValue(genderValues, genderValue));

        const facultyValues = await studentsPage.getColumnFilterValues(4, 0);
        const facultyValue = await studentsPage.getFilterValue(4, 0);
        expect(studentsPage.allArrayItemsEqualToValue(facultyValues, facultyValue));
    });

    it('row checked status should be the same as header checked status', () => {
        expect(studentsPage.isTableHeaderCheckboxChecked()).toBeFalsy('table header checkbox should be unactive');
        expect(studentsPage.expectAllCheckoxTobe(false)).toBeTruthy('all checkbox on rows should be unactive corresponding');
        studentsPage.clickTableHeaderCheckbox();
        expect(studentsPage.isTableHeaderCheckboxChecked()).toBeTruthy('table header checkbox should be active now');
        expect(studentsPage.expectAllCheckoxTobe(true)).toBeTruthy('all checkbox on rows should be active corresponding');
    });

    it('table row editing should be working', async () => {
        const row = studentsPage.getRow(1);
        const selectedStudentId = await studentsPage.getSelectedStudentId(row);
        const rowValues = studentsPage.getRowText(selectedStudentId);
        const editBtn = studentsPage.getEditBtn(row);
        const saveBtn = studentsPage.getSaveBtn(row);
        const cancelBtn = studentsPage.getCancelBtn(row);
        expect(studentsPage.isElementPresent(editBtn)).toBeTruthy();
        expect(studentsPage.isElementPresent(saveBtn)).toBeFalsy();
        expect(studentsPage.isElementPresent(cancelBtn)).toBeFalsy();

        studentsPage.clickOnElement(editBtn);
        studentsPage.inputSendKey('firstNameInRow', 'abc');
        studentsPage.inputSendKey('lastNameInRow', 'cba');
        studentsPage.selectDropdownItem('#genderInRow', true);
        studentsPage.selectDropdownItem('#facultyInRow', true);

        studentsPage.clickOnElement(saveBtn);
        studentsPage.refresh();

        const rowValuesAfterEdit = studentsPage.getRowText(selectedStudentId);
        expect(rowValuesAfterEdit).toEqual(rowValues);
    });
});
