import { Faculty } from '../page-objects/faculty.po';

describe('faculty page', () => {
    const facultyPage = new Faculty();
    beforeEach(() => {
        facultyPage.navigate();
    });

    it('table should be presented', () => {
        expect(facultyPage.isTablePresent()).toBeTruthy();
    });

    it('should have table caption text', () => {
        expect(facultyPage.getTableCaptionText()).toContain('Faculties');
    });

    it('should column toggle working', () => {
        expect(facultyPage.isColumnToggleMenuPresent()).toBeTruthy('column toggle object should be presented');
        expect(facultyPage.getColumnToggleMenuItems()).toEqual(['Id', 'Faculty Name', 'Training Time']);
    });

    it('should have table header', () => {
        expect(facultyPage.getTableHeader()).toEqual(['Id', 'Faculty Name', 'Training Time']);
    });

});
