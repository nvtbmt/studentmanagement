import { browser, by, element, promise} from 'protractor';

export class Faculty{

    navigate(): promise.Promise<any>{
        return browser.get('/faculties');
    }

    // table caption
    private getTableCaption(){
        return element(by.css('.ui-table-caption'));
    }
    getTableCaptionText(){
        return this.getTableCaption().getText();
    }
    private getColumnToggleMenu(){
        return element(by.css('.ui-multiselect'));
    }
    isColumnToggleMenuPresent(){
        return this.getColumnToggleMenu().isPresent();
    }
    getColumnToggleMenuItems(){
        const multiSelectTrigger = this.getTableCaption().element(by.css('.ui-multiselect-trigger'));
        multiSelectTrigger.click();
        return element.all(by.css('.ui-multiselect-item span.ng-star-inserted')).getText();
    }
    private getExcelDownloadButton(){
        return element(by.css('.main button[label="EXCEL"]'));
    }
    private getCsvDownloadButton(){
        return element(by.css('.main button[label="CSV"]'));
    }
    isDownloadButtonsPresent(){
        return this.getExcelDownloadButton().isPresent() && this.getCsvDownloadButton().isDisplayed();
    }
    checkFileDownloaded(){
        const filename = '../../downloads/export.csv';
        const fs = require('fs');
    }

    // table
    getTable(){
        return element(by.tagName('table'));
    }
    isTablePresent(){
        return this.getTable().isPresent();
    }
    getTableHeader(){
        return element.all(by.tagName('tr')).first().all(by.tagName('th')).getText();
    }
}
