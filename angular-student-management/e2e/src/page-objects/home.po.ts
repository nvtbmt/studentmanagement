import { browser, by, element, promise} from 'protractor';

export class Home{
    navigateToHome(): promise.Promise<any>{
        return browser.get('/');
    }

    // button
    getButton(){
        return element(by.css('.togglebtn'));
    }
    buttonCreated(){
        return this.getButton().isPresent();
    }
    clickButton(){
        return this.getButton().click();
    }

    // menu
    getShowedMenu(){
        return element(by.css('#menu-container .show'));
    }
    isShowedMenuPresent(){
        return this.getShowedMenu().isPresent();
    }
}
