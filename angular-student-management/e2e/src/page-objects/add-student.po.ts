import { $, $$, browser, by, element, ElementFinder, promise} from 'protractor';

export class AddStudent{

    navigate(): promise.Promise<any>{
        return browser.get('/students/addstudent');
    }

    getInput(inputId: string){
        return $(`#${inputId}`);
    }

    getInputValue(inputId: string){
        return $(`#${inputId}`).getAttribute('value');
    }

    sendKeyIntoInput(input: ElementFinder, value: string | number){
        input.sendKeys(value);
    }

    removeInputValue(input: ElementFinder){
        return input.clear();
    }

    clickRadioItem(itemId: string){
        element(by.css(`label[for='${itemId}']`)).click();
    }

    isSubmitButtonDisabled(){
        return element(by.css('button[type="submit"][disabled]')).isPresent();
    }

    clickSubmitButton(){
        element(by.css('button[type="submit"]')).click();
    }

    async checkIfCurrentUrl(destination: string){
        return browser.getCurrentUrl().then((currentUrl) => {
            return currentUrl.match(destination);
        });
    }

    async selectDropdownItem(dropdownSelector: string, indexOrAuto: number | boolean){
        const dropdown = element(by.css(dropdownSelector));
        const triggerButton = dropdown.element(by.css('.ui-dropdown-trigger'));
        triggerButton.click();
        if (typeof indexOrAuto === 'boolean'){
            return dropdown.$$('p-dropdownitem').each((elm, index) => {
                elm.$('li[aria-selected="false"]').isPresent().then((isPresent) => {
                    if (isPresent){
                        elm.click();
                    }
                });
            });
        }
        return dropdown.all(by.css('.ui-dropdown-item')).then((arr) => {
            if (indexOrAuto < arr.length && indexOrAuto >= 0){
                arr[indexOrAuto].click();
                return true;
            }
            return false;
        });
    }
}
