import { count } from 'console';
import { type } from 'os';
import { $, $$, browser, by, element, ElementFinder, promise} from 'protractor';

export class Student{

    navigate(): promise.Promise<any>{
        return browser.get('/students');
    }
    refresh(){
        return browser.refresh();
    }

    // input
    getInputValue(inputId: string){
        return $(`#${inputId}`).getAttribute('value');
    }

    // table
    private getTable(){
        return element(by.css('#container-table table'));
    }
    isTablePresent(){
        return this.getTable().isPresent();
    }

    // table caption
    private getTableCaption(){
        return element(by.css('.ui-table-caption'));
    }
    getTableCaptionText(){
        return this.getTableCaption().getText();
    }

    private getAddButton(){
        return element(by.css('.addButton'));
    }
    isAddButtonPresent(){
        return this.getAddButton().isPresent();
    }
    clickAddButton(){
        return this.getAddButton().click();
    }

    private getColumnToggleMenu(){
        return this.getTableCaption().element(by.css('.ui-multiselect'));
    }
    isColumnToggleMenuPresent(){
        return this.getColumnToggleMenu().isPresent();
    }
    getColumnToggleMenuItems(){
        const multiSelectTrigger = this.getTableCaption().element(by.css('.ui-multiselect-trigger'));
        multiSelectTrigger.click();
        return element.all(by.css('.ui-multiselect-item span.ng-star-inserted')).getText();
    }

    getTableHeader(){
        return element.all(by.tagName('thead tr')).first().all(by.tagName('th')).getText();
    }

    private getModalDialog(){
        return element(by.css('.main .ui-dialog'));
    }
    private getDialogForm(){
        return this.getModalDialog().element(by.css('.studentForm'));
    }
    isModalDialogPresent(){
        return this.getModalDialog().isPresent();
    }
    inputSendKey(inputId: string, value: string | number){
        element(by.id(inputId)).sendKeys(value);
    }
    clickRadioItem(itemId: string){
        element(by.css(`label[for='${itemId}']`)).click();
    }
    async selectDropdownItem(dropdownSelector: string, indexOrAuto: number | boolean){
        const dropdown = element(by.css(dropdownSelector));
        const triggerButton = dropdown.element(by.css('.ui-dropdown-trigger'));
        triggerButton.click();
        if (typeof indexOrAuto === 'boolean'){
            return dropdown.$$('p-dropdownitem').each((elm, index) => {
                elm.$('li[aria-selected="false"]').isPresent().then((isPresent) => {
                    if (isPresent){
                        elm.click();
                    }
                });
            });
        }
        return dropdown.all(by.css('.ui-dropdown-item')).then((arr) => {
            if (indexOrAuto < arr.length && indexOrAuto >= 0){
                arr[indexOrAuto].click();
                return true;
            }
            return false;
        });
    }
    isSubmitButtonDisabled(){
        return this.getModalDialog().element(by.css('.saveButton[disabled]')).isPresent();
    }
    clickSubmitButton(){
        this.getModalDialog().element(by.css('.saveButton')).click();
    }

    private getFilter(columnIndex: number){
        const filterBar = element(by.css('.filterbar'));
        const column = filterBar.all(by.tagName('th')).get(columnIndex);
        return column.element(by.css('.ui-multiselect'));
    }
    async getColumnFilterValues(columnIndex: number, filterItemIndex: number){
        const multiSelect = this.getFilter(columnIndex);
        const multiSelectTrigger = multiSelect.element(by.css('.ui-multiselect-trigger'));
        const tbody = this.getTable().element(by.tagName('tbody'));
        let filterValue: promise.Promise<string>;
        multiSelectTrigger.click();
        filterValue = multiSelect.all(by.css('.ui-multiselect-item span.ng-star-inserted')).get(filterItemIndex).getText();
        multiSelect.all(by.css('.ui-multiselect-item')).get(filterItemIndex).click();
        return await tbody.all(by.tagName(`tr td:nth-child(${columnIndex})`)).map<string>(async (column: ElementFinder) => {
            await column.getText();
        });
    }
    async getFilterValue(columnIndex: number, filterItemIndex: number){
        const multiSelect = this.getFilter(columnIndex);
        const multiSelectTrigger = multiSelect.element(by.css('.ui-multiselect-trigger'));
        multiSelectTrigger.click();
        return await multiSelect.all(by.css('.ui-multiselect-item span.ng-star-inserted')).get(filterItemIndex).getText();
    }
    allArrayItemsEqualToValue(array: string[], value: string){
        for (const item of array) {
            if (item !== value){
                return false;
            }
        }
        return true;
    }

    private getTableHeaderCheckbox(){
        return this.getTable().element(by.css('thead .ui-chkbox'));
    }
    isTableHeaderCheckboxChecked(){
        return this.getTableHeaderCheckbox().element(by.css(`[role='checkbox'][aria-checked='true']`)).isPresent();
    }
    clickTableHeaderCheckbox(){
        this.getTableHeaderCheckbox().element(by.css('[role="checkbox"]')).click();
    }
    async expectAllCheckoxTobe(expectValue: boolean){
        let result = false;
        await this.getTable().all(by.css(`tbody td [role='checkbox'][aria-checked='${!expectValue}']`)).count().then((total) => {
            result = total === 0 ? true : false;
        });
        return result;
    }

    getRow(rowIndex: number){
        return this.getTable().$(`tbody tr:nth-child(${rowIndex})`);
    }
    gettext(row: ElementFinder){
        return row.getText();
    }
    getEditBtn(row: ElementFinder){
        return row.$('.rowEditing .editBtn');
    }
    getSaveBtn(row: ElementFinder){
        return row.$('.rowEditing .saveBtn');
    }
    getCancelBtn(row: ElementFinder){
        return row.$('.rowEditing .cancelBtn');
    }
    isElementPresent(elementFinder: ElementFinder){
        return elementFinder.isPresent();
    }
    clickOnElement(elementFinder: ElementFinder){
        elementFinder.click();
    }
    getSelectedStudentId(row: ElementFinder){
        return row.$$('td').first().getText();
    }
    getRowText(studentId: string){
        return this.getTable().$$('tbody tr').each(async (elm, index) => {
            return elm.$$('td').first().getText().then((text) => {
                if (text === studentId){
                    return elm.getText();
                }
            });
        });
    }

    navigateToDetail(rowIndex: number){
        const editLink = this.getTable().$$('tbody tr').get(rowIndex).$$('td').get(7).$('a');
        editLink.click();
    }
}
