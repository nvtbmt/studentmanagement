import { browser, by, element, promise} from 'protractor';

export class Template{

    navigate(): promise.Promise<any>{
        return browser.get('/');
    }

}
